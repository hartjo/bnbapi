-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: dbbnb
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beneplace`
--

DROP TABLE IF EXISTS `beneplace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beneplace` (
  `sessionid` varchar(500) NOT NULL,
  `centerid` varchar(500) DEFAULT NULL,
  `confirmationnumber` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL,
  `day` date DEFAULT NULL,
  `hour` varchar(500) DEFAULT NULL,
  `comment` text,
  `program` varchar(500) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `centersessionstatus` int(11) DEFAULT NULL,
  `regionsessionstatus` int(11) DEFAULT NULL,
  `adminsessionstatus` int(11) DEFAULT NULL,
  `device` varchar(500) DEFAULT NULL,
  `deviceos` varchar(500) DEFAULT NULL,
  `devicebrowser` varchar(500) DEFAULT NULL,
  `payment` varchar(500) DEFAULT NULL,
  `paid` double DEFAULT NULL,
  PRIMARY KEY (`sessionid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneplace`
--

LOCK TABLES `beneplace` WRITE;
/*!40000 ALTER TABLE `beneplace` DISABLE KEYS */;
INSERT INTO `beneplace` VALUES ('1BDC6B8B-0BC3-43E1-AB2A-FAF5D0E29F01','05B92C56-1D9D-4BDF-91BF-BF4E513D65BB','45338031EV864640B','Nancy Oliver','nancy@olivertribe.com','(703) 231-4574','2015-09-25','01:30 PM','Tai chi has been recommended by my chiropractor. I have had both knees and both hips replaced and my balance is terrible. I may need to have individu',NULL,NULL,'2015-09-22 06:06:13',0,0,1,NULL,NULL,NULL,'Paypal',15),('D7DBE5C4-93AC-4D9F-B9DE-E12E11BCFBBC','A741D122-B3AD-4E51-9E91-9F7AC4207C45','18L97176RM245122R','Sue Davis','sue.davis5@frontier.com','425-218-0594','2015-10-22','04:30 PM','I have spinal stenosis and recently have had a pinched nerve in my spinal canal that has been dibilitating.  I am seeing a chiropractor for the first ti',NULL,NULL,'2015-10-14 13:16:51',0,0,1,NULL,NULL,NULL,'Paypal',15);
/*!40000 ALTER TABLE `beneplace` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-21 11:02:17
