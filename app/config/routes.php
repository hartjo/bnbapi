<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */


$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/test/{id}',
	'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
	'method' => 'post',
	'route' => '/skip/{name}',
	'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/marker/upload',
    'handler' => ['Controllers\MapController', 'uploadPicsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/create/info',
    'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
    'authentication' => FALSE
];

//rainier routers

//pages routers
$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveimage',
    'handler' => ['Controllers\PagesController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listimages',
    'handler' => ['Controllers\PagesController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/create',
    'handler' => ['Controllers\PagesController', 'createPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepage/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PagesController', 'managePagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagestatus/{status}/{pageid}/{keyword}',
    'handler' => ['Controllers\PagesController', 'pageUpdatestatusAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/page/pagedelete/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pageedit/{pageid}',
    'handler' => ['Controllers\PagesController', 'pageeditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pagerightitem/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagerightitemAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveeditedpage',
    'handler' => ['Controllers\PagesController', 'saveeditedPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpage/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPageAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/leftsidebarid/{pageid}',
 'handler' => ['Controllers\PagesController', 'leftsidebaridAction'],
 'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/page/leftsidebar/{offset}/{sidebarid}',
 'handler' => ['Controllers\PagesController', 'listleftsidebarAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/rightsidebar/{offset}/{pageid}',
 'handler' => ['Controllers\PagesController', 'listrightsidebarAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/initialawakening',
 'handler' => ['Controllers\PagesController', 'pageinitialawakeningAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/related/{pageslugs}',
 'handler' => ['Controllers\PagesController', 'pagerelatedAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/page/view/{pageslugs}',
 'handler' => ['Controllers\PagesController', 'fepageviewAction'],
 'authentication' => FALSE
];



//news routers
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveimage',
    'handler' => ['Controllers\NewsController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listimages',
    'handler' => ['Controllers\NewsController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listcategory',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'createNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/create',
    'handler' => ['Controllers\NewsController', 'createNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managenews/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'manageNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewscenterstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsCenterUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newsdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newscenterdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscenterdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newsedit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newseditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newscenteredit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscentereditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/edit',
    'handler' => ['Controllers\NewsController', 'editNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/edit',
    'handler' => ['Controllers\NewsController', 'editNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/createnews/workshoptitles',
    'handler' => ['Controllers\NewsController', 'workshoptitlesAction'],
    'authentication' => FALSE
];



$routes[] = [
    'method' => 'get',
    'route' => '/news/managecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savecategory',
    'handler' => ['Controllers\NewsController', 'createcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategorynames',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/validate/categ/{categ}',
    'handler' => ['Controllers\NewsController', 'validatecategAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/categorydelete/{id}',
    'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listcategory',
 'handler' => ['Controllers\NewsController', 'listcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/randomcategoryfrontend',
 'handler' => ['Controllers\NewsController', 'randomcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/featurednews',
 'handler' => ['Controllers\NewsController', 'featurednewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/latest',
 'handler' => ['Controllers\NewsController', 'latestnewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/founderswisdom',
 'handler' => ['Controllers\NewsController', 'founderswisdomfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/showcategoryname/{categoryslugs}',
 'handler' => ['Controllers\NewsController', 'showcategorynameAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnewsbycategory/{categoryslugs}/{offset}',
 'handler' => ['Controllers\NewsController', 'listnewsbycategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/showtotalnewscategoryitem/{categoryslugs}',
 'handler' => ['Controllers\NewsController', 'showtotalnewscategoryitemAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/deletenewsimg/{id}',
 'handler' => ['Controllers\NewsController', 'deletenewsimgAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/listcenter',
 'handler' => ['Controllers\NewsController', 'socialUpdatestatusAction'],
 'authentication' => FALSE
];
//validate news if already taken
$routes[] = [
 'method' => 'get',
 'route' => '/news/validate/{title}',
 'handler' => ['Controllers\NewsController', 'Validatenewstitlesction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/newsslugs/validate/{slugs}/{centerid}',
 'handler' => ['Controllers\NewsController', 'ValidateNewsSlugsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/index',
 'handler' => ['Controllers\NewsController', 'febnbbuzzindexAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'febnbbuzzviewAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/category/{categoryslugs}/{offset}',
 'handler' => ['Controllers\NewsController', 'febnbbuzzcategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/regionmanagerlist',
 'handler' => ['Controllers\CenterController', 'listregionmanagerAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/listregion',
 'handler' => ['Controllers\CenterController', 'listregionAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/editcenter/districtlist',
 'handler' => ['Controllers\CenterController', 'editlistdistrictAction'],
 'authentication' => FALSE
];



$routes[] = [
 'method' => 'get',
 'route' => '/center/centermanagerlist',
 'handler' => ['Controllers\CenterController', 'listcentermanagerAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/statelist',
 'handler' => ['Controllers\CenterController', 'liststateAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/citylist/{statecode}',
 'handler' => ['Controllers\CenterController', 'listcityAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/ziplist/{cityname}',
 'handler' => ['Controllers\CenterController', 'getzipAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'post',
 'route' => '/center/createcenter',
 'handler' => ['Controllers\CenterController', 'savecenterAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/managecenter/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\CenterController', 'manageCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/managecenterdropdown/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\CenterController', 'dropdownmanageCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updatecenterstatus/{status}/{centerid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'centerUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/centerdelete/{centerid}',
    'handler' => ['Controllers\CenterController', 'centerdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadcenter/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveeditedcenter',
    'handler' => ['Controllers\CenterController', 'saveeditedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/calendar/createevent',
    'handler' => ['Controllers\CenterController', 'saveEventAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/calendar/listevent/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\CenterController', 'manageEventAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateeventstatus/{status}/{activityid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'eventUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/eventdelete/{activityid}',
    'handler' => ['Controllers\CenterController', 'eventdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/calendar/loadevent/{activityid}',
    'handler' => ['Controllers\CenterController', 'loadEventAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/calendar/updateevent',
    'handler' => ['Controllers\CenterController', 'eventupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/location/savelocation',
    'handler' => ['Controllers\CenterController', 'savelocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/location/loadlocation/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadLocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/openhours/saveopenhour',
    'handler' => ['Controllers\CenterController', 'saveopenhourAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/openhours/loadhour/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadopenhourAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/savesocial',
    'handler' => ['Controllers\CenterController', 'savesociallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/social/loadsocial/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadsocialAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/deletesocial/{linkid}',
    'handler' => ['Controllers\CenterController', 'deletesociallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/updatesocialstatus/{status}/{linkid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'socialUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerview/loadcenter/{centerid}',
    'handler' => ['Controllers\CenterController', 'centerviewloadcenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/social/loadlinksbyid/{linkid}',
    'handler' => ['Controllers\CenterController', 'loadlinksbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/updatesociallink',
    'handler' => ['Controllers\CenterController', 'updatesociallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/updatesocialurlstatus',
    'handler' => ['Controllers\CenterController', 'updatesocialurlstatusAction'],
    'authentication' => FALSE
];



$routes[] = [
    'method' => 'post',
    'route' => '/center/social/updatelinksbyid',
    'handler' => ['Controllers\CenterController', 'updatelinksbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/spnl/{centerid}',
    'handler' => ['Controllers\CenterController', 'spnlAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/setspnl/{centerid}/{spnlstatus}',
    'handler' => ['Controllers\CenterController', 'setspnlAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/saveoffer',
    'handler' => ['Controllers\CenterController', 'saveofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateoffer',
    'handler' => ['Controllers\CenterController', 'updateofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateofferwithupload',
    'handler' => ['Controllers\CenterController', 'updateofferWithUploadAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/loadoffer/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/viewreferrallink',
    'handler' => ['Controllers\BeneplaceController', 'viewreferrallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/beneplace/savereferral',
    'handler' => ['Controllers\BeneplaceController', 'addreferralAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/deleteoffer/{offerid}',
    'handler' => ['Controllers\CenterController', 'deleteofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/editoffer/{offerid}',
    'handler' => ['Controllers\CenterController', 'editofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadfee/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadfeeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savefee',
    'handler' => ['Controllers\CenterController', 'savefeeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statusregfee/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statusregfeeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statusregclass/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statusregclassAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statussessionfee/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statussessionfeeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savemembership',
    'handler' => ['Controllers\CenterController', 'savemembershipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadmembership/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadmembershipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadmembershipbyid/{membershipid}',
    'handler' => ['Controllers\CenterController', 'loadmembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savemembershipbyid',
    'handler' => ['Controllers\CenterController', 'savemembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/deletemembershipbyid/{membershipid}',
    'handler' => ['Controllers\CenterController', 'deletemembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadprivatesession/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadprivatesessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/saveprivatesession',
    'handler' => ['Controllers\CenterController', 'saveprivatesessionAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/loadschedule/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadscheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/schedule/saveschedule',
    'handler' => ['Controllers\CenterController', 'savescheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/loadschedulebyid/{sessionid}',
    'handler' => ['Controllers\CenterController', 'loadschedulebyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/schedule/updateschedule',
    'handler' => ['Controllers\CenterController', 'updatescheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/deleteschedule/{sessionid}',
    'handler' => ['Controllers\CenterController', 'deletescheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/news/newslist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\CenterController', 'newslistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/phone/getcenterphonenumber/{centerid}',
    'handler' => ['Controllers\CenterController', 'getcenterphonenumberAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/phone/savecenterphonenumber',
    'handler' => ['Controllers\CenterController', 'savecenterphonenumberAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateofferstatus/{status}/{offerid}',
    'handler' => ['Controllers\CenterController', 'offerUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listnewsbycenterAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/fullnewsbycenter/{centerslugs}/{newsslugs}',
 'handler' => ['Controllers\CenterController', 'fullnewsbycenterAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listsuccesssotriesbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listsuccesssotriesbycenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listeventsbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listeventsbycentercenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveregion',
    'handler' => ['Controllers\CenterController', 'saveregionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadregion/{num}/{off}/{keyword}',
    'handler' => ['Controllers\CenterController', 'loadregionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/deleteregion',
    'handler' => ['Controllers\CenterController', 'deleteregionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateregion',
    'handler' => ['Controllers\CenterController', 'updateregionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/savedistrict',
    'handler' => ['Controllers\CenterController', 'savedistrictAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadallregion',
    'handler' => ['Controllers\CenterController', 'loadallregionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loaddistrict/{num}/{off}/{keyword}',
    'handler' => ['Controllers\CenterController', 'loaddistrictAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/deletedistrict',
    'handler' => ['Controllers\CenterController', 'deletedistrictAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/loaddistrictbyid',
    'handler' => ['Controllers\CenterController', 'loaddistrictbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateistrict',
    'handler' => ['Controllers\CenterController', 'updateistrictAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/email/getcenteremail/{centerid}',
    'handler' => ['Controllers\CenterController', 'getcenteremailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/email/saveemail',
    'handler' => ['Controllers\CenterController', 'saveemailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerpagedata/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'centerpagedataAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/fe/getstarted/searchlocation',
    'handler' => ['Controllers\GetstartedController', 'searchlocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/location/searchcenterlocation',
    'handler' => ['Controllers\CenterlocationController', 'searchlocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/getstarted/paypalreturn',
    'handler' => ['Controllers\GetstartedController', 'paypalreturnAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/introsessionlist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'introsessionlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/getstarted/authorize',
    'handler' => ['Controllers\GetstartedController', 'authorizeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/getcenterdetails/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'getcenterdetailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadsession/{sessionid}/{userid}',
    'handler' => ['Controllers\GetstartedController', 'loadsessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/groupclasssessionlist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'groupclasssessionlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadgroupsession/{sessionid}/{userid}',
    'handler' => ['Controllers\GetstartedController', 'loadgroupsessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/price/loadprice',
    'handler' => ['Controllers\CenterController', 'loadpriceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/price/loadbeneplaceprice',
    'handler' => ['Controllers\CenterController', 'loadbeneplacepriceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/price/saveintro',
    'handler' => ['Controllers\CenterController', 'saveintroAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/price/beneplacesaveintro',
    'handler' => ['Controllers\CenterController', 'beneplacesaveintroAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/getstarted/loadcenter/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'loadcenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/beneplacelist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\BeneplaceController', 'beneplacelistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/loadbeneplacesession/{sessionid}/{userid}',
    'handler' => ['Controllers\BeneplaceController', 'loadbeneplacesessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allbeneplace/allbeneplacelist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\BeneplaceController', 'allbeneplacelistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allintrosession/allintrosession/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allintrosessionlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allgroupclasssession/allgroupclasssession/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allgroupclasssessionlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/managercontract/savecontact',
    'handler' => ['Controllers\CenterController', 'savecontactAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/managercontract/loadcontact/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadcontacttAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/managercontract/testsmsmessage',
    'handler' => ['Controllers\CenterController', 'testsmsmessageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/dashboard/counttotalpost',
    'handler' => ['Controllers\DashboardController', 'counttotalpostAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/getip/visit/{ip}',
    'handler' => ['Controllers\VisitController', 'visitAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/dashboard/countvisit',
    'handler' => ['Controllers\VisitController', 'countAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/listnotification/{userid}/{offset}/{filter}',
    'handler' => ['Controllers\NotificationController', 'listnotificationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/changestatus/{notificationid}/{userid}',
    'handler' => ['Controllers\NotificationController', 'changestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/notificationcount/{userid}',
    'handler' => ['Controllers\NotificationController', 'notificationcountAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/notificationcountzero/{userid}',
    'handler' => ['Controllers\NotificationController', 'notificationcountzeroAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/loadnotification/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\NotificationController', 'loadnotificationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/deletenotification/{notificationid}',
    'handler' => ['Controllers\NotificationController', 'deletenotificationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/auditlogs/auditloglist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\AuditlogController', 'auditloglistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/centeroffer/offer',
    'handler' => ['Controllers\CenterController', 'getofferAction'],
    'authentication' => FALSE
];



/** NEIL ROUTER

**/
$routes[] = [
 'method' => 'get',
 'route' => '/fe/index/properties',
 'handler' => ['Controllers\IndexController', 'FEindexPropertiesAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'showFullNewsAction'],
 'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/sidebarmostviewednews',
 'handler' => ['Controllers\NewsController', 'showSidebarMostViewedAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/sidebarlatestnews',
 'handler' => ['Controllers\NewsController', 'showSidebarLatestNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updateviews/{newsslugs}',
    'handler' => ['Controllers\NewsController', 'newsViewsCountUpdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/popularnews',
    'handler' => ['Controllers\NewsController', 'displayPopularNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/rss/{offset}',
 'handler' => ['Controllers\NewsController', 'newrssAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mainnews/workshoptitles/{newsid}',
    'handler' => ['Controllers\NewsController', 'mainnewsworkshoprelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mainnews/addrelated',
    'handler' => ['Controllers\NewsController', 'mainnewsaddrelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/mainnews/removerelated',
    'handler' => ['Controllers\NewsController', 'mainnewsremoverelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centernews/workshoprelated/{newsid}',
    'handler' => ['Controllers\NewsController', 'centernewsworkshoprelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centernews/addrelated',
    'handler' => ['Controllers\NewsController', 'centernewsaddrelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centernews/removerelated',
    'handler' => ['Controllers\NewsController', 'centernewsremoverelatedAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/user/setstatus/{userid}/{userstatus}',
    'handler' => ['Controllers\UserController', 'userSetStatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/centerlist',
    'handler' => ['Controllers\CenterController', 'listOfCentersAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/statelist',
    'handler' => ['Controllers\CenterController', 'liststateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/success-stories/publish',
    'handler' => ['Controllers\TestimoniesController', 'publishAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerlistviastate/{state_code}',
    'handler' => ['Controllers\CenterController', 'centerListViaStateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/storieslist',
    'handler' => ['Controllers\TestimoniesController', 'storiesListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/managestories/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimoniesController', 'manageStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/managestoriespercenter/{centerid}/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimoniesController', 'manageStoriesPerCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/information/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'getStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/indexpage/{offset}',
    'handler' => ['Controllers\TestimoniesController', 'feindexAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/tagpage/{tag}/{offset}',
    'handler' => ['Controllers\TestimoniesController', 'fetagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/savereviewedstory',
    'handler' => ['Controllers\TestimoniesController', 'saveReviewedStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/delete/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'deleteStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/spotlight',
    'handler' => ['Controllers\TestimoniesController', 'spotlightAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/totallateststories/{offset}',
    'handler' => ['Controllers\TestimoniesController', 'totalLatestStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/frontend/showtotallateststoriesitem',
    'handler' => ['Controllers\TestimoniesController', 'showTotalLatestStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/details/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'viewStoryDetailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/index/locations/states',
    'handler' => ['Controllers\LocationsController', 'locationStatesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveimagesforslider',
    'handler' => ['Controllers\CenterController', 'centerSaveImagesForSliderAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/view',
    'handler' => ['Controllers\NewsController', 'viewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/center/loadcenterimages/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadCenterImagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerslider/delete/{img}',
    'handler' => ['Controllers\CenterController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/page/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/getlocation',
    'handler' => ['Controllers\CenterController', 'loadCenterPagelocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/successstories/{centerslugs}/{storyslugs}/{ssid}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/centerinfo/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageInfoAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/classschedule/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageClassScheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/pricing/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPagePricingAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/loginuser/centermanager/{username}',
    'handler' => ['Controllers\UserController', 'loginCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/available/regdiscen/{userid}',
    'handler' => ['Controllers\UserController', 'availableRegDisCenListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/success/write/statelist',
    'handler' => ['Controllers\CenterController', 'FEloadstateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/success-stories/view/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'FEsuccessstoriespageviewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/addmetatags',
    'handler' => ['Controllers\TestimoniesController', 'storyaddmetatagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/workshoptitles/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'workshoptitlesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/addrelated',
    'handler' => ['Controllers\TestimoniesController', 'storyaddrelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/removerelated',
    'handler' => ['Controllers\TestimoniesController', 'storyremoverelatedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/metatags/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'storymetatagsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/updatemetatags',
    'handler' => ['Controllers\TestimoniesController', 'storyupdatemetatagAction'],
    'authentication' => FALSE
];



$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/createvenue',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreatevenueAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/listofvenues/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListofvenuesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/checkvenuename/{venuename}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCheckvenuenameAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/deleteworkshopvenue/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopDeleteworkshopvenueAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/editworkshopvenue/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopEditworkshopvenueAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/checkvenuenamewexception/{venuename}/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCheckvenuenamewexceptionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/updatevenue/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdatevenueAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/createtitle',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreatetitleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/listoftitles/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListoftitlesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/updatetitle',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdatetitleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/removetitle/{titleid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopremovetitleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/checktitlename/{titleslugs}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopChecktitlenameAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/lists',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/createworkshop',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreateworkshopAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/listofworkshop/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListofworkshopAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/removeworkshop/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopRemoveworkshopAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/edit/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopEditworkshopAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/BE/workshop/update/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdateworkshopAction'],
    'authentication' => FALSE
];

// $routes[] = [
//     'method' => 'post',
//     'route' => '/BE/workshop/update/{workshopid}',
//     'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdateworkshopAction'],
//     'authentication' => FALSE
// ];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/lists',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListAction'], //acting like universal action :]
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/loadcity/viastatecode/{statecode}',
    'handler' => ['Controllers\WorkshopController', 'loadcityViastatecodeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/workshopschedules/{titleslugs}/{offset}',
    'handler' => ['Controllers\WorkshopController', 'workshopschedulePaginationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/schedule/pagination',
    'handler' => ['Controllers\WorkshopController', 'workshopschedulepaginationangularAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/schedulelist/{titleslugs}/{offset}',
    'handler' => ['Controllers\WorkshopController', 'workshopSchedulelistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/detail/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshopdetailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/location/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshoplocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/registrationinfo/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshopdetailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/registrant',
    'handler' => ['Controllers\WorkshopController', 'registrantAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/registrantslist/{num}/{off}',
    'handler' => ['Controllers\WorkshopController', 'registrantslistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/removeregistrant/{registrantid}',
    'handler' => ['Controllers\WorkshopController', 'removeRegistrantAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/overview',
    'handler' => ['Controllers\WorkshopController', 'overviewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/related',
    'handler' => ['Controllers\WorkshopController', 'relatedAction'],
    'authentication' => FALSE
];

// WORKSHOP ^

$routes[] = [
    'method' => 'get',
    'route' => '/sidebar/contents',
    'handler' => ['Controllers\SidebarController', 'contentsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/oneonone',
    'handler' => ['Controllers\CenterincomeController','oneononeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filteroneonone',
    'handler' => ['Controllers\CenterincomeController','filteroneononeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/groupsession',
    'handler' => ['Controllers\CenterincomeController','groupsessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filtergroupsession',
    'handler' => ['Controllers\CenterincomeController','filtergroupsessionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/chartperday',
    'handler' => ['Controllers\CenterincomeController','chartperdayAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/chartpermonth',
    'handler' => ['Controllers\CenterincomeController','chartpermonthAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterchartperday',
    'handler' => ['Controllers\CenterincomeController','filterchartperdayAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterchartpermonth',
    'handler' => ['Controllers\CenterincomeController','filterchartpermonthAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/oneononefranchise',
    'handler' => ['Controllers\CenterincomeController','oneononefranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/allfranchise',
    'handler' => ['Controllers\CenterincomeController','allfranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterallfranchise',
    'handler' => ['Controllers\CenterincomeController','filterallfranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filteroneononefranchise',
    'handler' => ['Controllers\CenterincomeController','filteroneononefranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/groupfranchise',
    'handler' => ['Controllers\CenterincomeController','groupfranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filtergroupfranchise',
    'handler' => ['Controllers\CenterincomeController','filtergroupfranchiseAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/details/{centerid}',
    'handler' => ['Controllers\CenterController','detailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/savedescription',
    'handler' => ['Controllers\CenterController','savedescriptionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveordering',
    'handler' => ['Controllers\CenterController','saveorderingAction'],
    'authentication' => FALSE
];




/*NEIL ENDS*/



/** Kyben
 */
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/username/{name}',
    'handler' => ['Controllers\UserController', 'userExistAction'],
    'authentication' => FALSE
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/email/{email}',
    'handler' => ['Controllers\UserController', 'emailExistAction'],
    'authentication' => FALSE
];
/* Submit User Regester */
$routes[] = [
    'method' => 'post',
    'route' => '/user/register',
    'handler' => ['Controllers\UserController', 'registerUserAction'],
    'authentication' => FALSE
];

/* List all User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\UserController', 'userListAction'],
    'authentication' => FALSE
];
/* User Info */
$routes[] = [
    'method' => 'get',
    'route' => '/user/info/{id}',
    'handler' => ['Controllers\UserController', 'userInfoction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/update',
    'handler' => ['Controllers\UserController', 'userUpdateAction'],
    'authentication' => FALSE
];
/* DELETE User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/delete/{id}',
    'handler' => ['Controllers\UserController', 'deleteUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\UserController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/login/{username}/{password}',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/beneplace/trialaccount/{zipcode}/{textcity}/{state}/{lat}/{lon}',
    'handler' => ['Controllers\BeneplaceController', 'benePlaceAction'],
    'authentication' => FALSE
];



/**SIMULA NG ROTA NI RYAN JERIC.................. CONTACT US**/

$routes[] = [
    'method' => 'post',
    'route' => '/contactsend/form',
    'handler' => ['Controllers\ContactusController', 'sendAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/list/{num}/{off}/{keyword}/{status}',
    'handler' => ['Controllers\ContactusController', 'listcontactsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/messagedelete/{id}',
    'handler' => ['Controllers\ContactusController', 'messagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/listreplies/{id}',
    'handler' => ['Controllers\ContactusController', 'listreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/message/view/{id}',
    'handler' => ['Controllers\ContactusController', 'viewreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/message/reply',
    'handler' => ['Controllers\ContactusController', 'replyrequestAction'],
    'authentication' => FALSE
];


/**SIMULA NG ROTA ng FORGOT PASSWORD <Ryanjeric>**/
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/send/{email}',
    'handler' => ['Controllers\UserController', 'forgotpasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/checktoken/check/{email}/{token}',
    'handler' => ['Controllers\UserController', 'checktokenAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/updatepassword/token',
    'handler' => ['Controllers\UserController', 'updatepasswordtokenAction'],
    'authentication' => FALSE
];


//SIMULA NG ROTA ng News Letter - Subscribers <Ryanjeric>

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/emailcheck/{email}',
    'handler' => ['Controllers\SubscribersController', 'checkemailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/subscriber/add',
    'handler' => ['Controllers\SubscribersController', 'addsubscriberAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\SubscribersController', 'subscriberslistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/delete/{id}',
    'handler' => ['Controllers\SubscribersController', 'deletesubscriberAction'],
    'authentication' => FALSE
];

// $routes[] = [
//     'method' => 'get',
//     'route' => '/delete/transaction',
//     'handler' => ['Controllers\WorkshopController', 'deletetransactionAction'],
//     'authentication' => FALSE
// ];


//SIMULA NG ROTA ng News Letter - Newsletter <Ryanjeric>



//IMAGE UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/saveimage',
    'handler' => ['Controllers\NewsletterController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/listimages',
    'handler' => ['Controllers\NewsletterController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/delete/{imgid}',
    'handler' => ['Controllers\NewsletterController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/add',
    'handler' => ['Controllers\NewsletterController', 'savenewsletterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/checktitle/{title}',
    'handler' => ['Controllers\NewsletterController', 'checktitleAction'],
    'authentication' => FALSE
];

//PDF UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/savepdf',
    'handler' => ['Controllers\NewsletterController', 'savepdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/pdffiles',
    'handler' => ['Controllers\NewsletterController', 'listpdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/pdfdelete/{imgid}',
    'handler' => ['Controllers\NewsletterController', 'deletepdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managenewsletter/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsletterController', 'manageNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/deletenewspdf/{id}',
    'handler' => ['Controllers\NewsletterController', 'deletenewspdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/deletenewsletter/{id}',
    'handler' => ['Controllers\NewsletterController', 'deletenewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/changestatus/{stat}/{id}',
    'handler' => ['Controllers\NewsletterController', 'changestatus1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletterpdf/changestatus/{stat}/{id}',
    'handler' => ['Controllers\NewsletterController', 'changestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/edit/{id}',
    'handler' => ['Controllers\NewsletterController', 'newslettereditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/editpdf/{id}',
    'handler' => ['Controllers\NewsletterController', 'newspdfeditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/updatenewsletter',
    'handler' => ['Controllers\NewsletterController', 'updateNewsletterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/updatenewsletterpdf',
    'handler' => ['Controllers\NewsletterController', 'updateNewspdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pdf/pdfcheck/{filename}',
    'handler' => ['Controllers\NewsletterController', 'checkfileAction'],
    'authentication' => FALSE
];

//NEWS LETTER FRONT END <3
$routes[] = [
    'method' => 'get',
    'route' => '/fe/enewsletter/popularnews/listsuccesssotries',
    'handler' => ['Controllers\NewsletterController', 'loadStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/enewslettershow/{id}',
    'handler' => ['Controllers\NewsletterController', 'fenewslettershowAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/fe/newsletterlist/{num}/{off}',
    'handler' => ['Controllers\NewsletterController', 'fenewsletterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/pdfnewsletterlist/{num}/{off}',
    'handler' => ['Controllers\NewsletterController', 'fepdfnewsletterAction'],
    'authentication' => FALSE
];


//    Settings - Simula ng ROTA ni Ryanjeric


$routes[] = [
    'method' => 'get',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceon',
    'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceoff',
    'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/uploadlogo',
    'handler' => ['Controllers\SettingsController', 'uploadlogoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/settings/logolist',
    'handler' => ['Controllers\SettingsController', 'listlogoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/delete/{img}',
    'handler' => ['Controllers\SettingsController', 'deletelogoAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/savedefaultlogo',
    'handler' => ['Controllers\SettingsController', 'savedefaultlogoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/googleanalytics',
    'handler' => ['Controllers\SettingsController', 'scriptAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/loadscript',
    'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/script',
    'handler' => ['Controllers\SettingsController', 'displaytAction'],
    'authentication' => FALSE
];

// GHOST ROUTES
$routes[] = [
    'method' => 'post',
    'route' => '/user/changepassword',
    'handler' => ['Controllers\UserController', 'changepasswordAction'],
    'authentication' => FALSE
];


return $routes;
