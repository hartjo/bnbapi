<?php

namespace Controllers;

use \Models\Workshop as Workshop;
use \Models\Workshopassociatedcenter as Workshopassociatedcenter;
use \Models\Workshopvenue as Workshopvenue;
use \Models\Notification as Notification;
use \Models\Workshoptitle as Workshoptitle;
use \Models\Workshopregistrant as Workshopregistrant;
use \Models\Center as Center;
use \Models\States as States;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Models\News as News;
use \Models\Centernews as Centernews;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class WorkshopController extends \Phalcon\Mvc\Controller {

	public function BEworkshopCreatevenueAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {

			$guid = new \Utilities\Guid\Guid();
			$workshopvenueid = $guid->GUID();
			$venuename = $request->getPost('venuename');
			$venueslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
			$city = $request->getPost('city');
			$SPR = $request->getPost('SPR');
			$zipcode = $request->getPost('zipcode');
			$country = $request->getPost('country');
			$website = $request->getPost('website');
			$address1 = $request->getPost('address1');
			$address2 = $request->getPost('address2');
			$latitude = $request->getPost('latitude');
			$longtitude = $request->getPost('longtitude');
			$contact = $request->getPost('contact');
			$authorizeid = $request->getPost('authorizeid');
			$authorizekey = $request->getPost('authorizekey');

			$workshopvenue = new Workshopvenue();
			$workshopvenue->assign(array(
				"workshopvenueid" => $workshopvenueid,
				"venuename" => $venuename,
				"venueslugs" => $venueslugs,
				"city" => $city,
				"SPR" => $SPR,
				"zipcode" => $zipcode,
				"country" => $country,
				"website" => $website,
				"address1" => $address1,
				"address2" => $address2,
				"latitude" => $latitude,
				"longtitude" => $longtitude,
				"vauthorizeid" => $authorizeid,
				"vauthorizekey" => $authorizekey,
				"contact" => $contact,
				"date_created" => date('Y-m-d'),
				"date_updated" => date('Y-m-d H:i:s')
			));

			if($workshopvenue->create()) {
				echo json_encode(array("result"=>"Successful!"));
			} else {
					$errors = array();
					foreach($workshopvenue->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
				echo json_encode(array("result"=>$errors, "venueslugs" => $venueslugs));
			}
		}
	}

	public function BEworkshopListofvenuesAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT * FROM workshopvenue LIMIT $offsetfinal, $num");
			$stmt->execute();
			$venues = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$venuescount = Workshopvenue::find();

			$totalNumberOfVenues = count($venuescount);

		} else {
			$stmt = $db->prepare("SELECT * FROM workshopvenue WHERE venuename LIKE '%$keyword%' LIMIT $offsetfinal, $num");
			$stmt->execute();
			$venues = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshopvenue WHERE venuename LIKE '%$keyword%' ");
			$stmt->execute();
			$venuescount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfVenues = count($venuescount);
		}

		echo json_encode(array('data' => $venues, 'index' => $page, 'total_items' => $totalNumberOfVenues));

	}

	public function BEworkshopCheckvenuenameAction($venuename) {
		$slugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
		$venue= Workshopvenue::findFirst("venueslugs = '$slugs' ");
		if($venue) {
			echo json_encode(array("exists" => true));
		} else {
			echo json_encode(array("exists" => false));
		}
	}

	public function BEworkshopDeleteworkshopvenueAction($venueid) {
		$workshopvenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
		if($workshopvenue) {
			if($workshopvenue->delete()) {
				echo json_encode(array("result" => "Successfully Deleted!"));
			} else {
				echo json_encode(array("result" => "There was an error!"));
			}
		} else {
			echo json_encode(array("result" => "There was an error!"));
		}
	}

	public function BEworkshopEditworkshopvenueAction($venueid) {
		$workshopvenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
		echo json_encode($workshopvenue);
	}

	public function BEworkshopCheckvenuenamewexceptionAction($venuename,$venueid) {
		$slugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
		$venue= Workshopvenue::findFirst("venueslugs = '$slugs' AND workshopvenueid != '$venueid' ");
		if($venue) {
			echo json_encode(array("exists" => true));
		} else {
			echo json_encode(array("exists" => false));
		}
	}

	public function BEworkshopUpdatevenueAction($venueid) {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {

			$venuename = $request->getPost('venuename');
			$venueslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
			$city = $request->getPost('city');
			$SPR = $request->getPost('SPR');
			$zipcode = $request->getPost('zipcode');
			$country = $request->getPost('country');
			$website = $request->getPost('website');
			$address1 = $request->getPost('address1');
			$address2 = $request->getPost('address2');
			$latitude = $request->getPost('latitude');
			$longtitude = $request->getPost('longtitude');
			$contact = $request->getPost('contact');
			$authorizeid = $request->getPost('vauthorizeid');
			$authorizekey = $request->getPost('vauthorizekey');

			$updatevenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
			$updatevenue->assign(array(
				"venuename" => $venuename,
				"venueslugs" => $venueslugs,
				"city" => $city,
				"SPR" => $SPR,
				"zipcode" => $zipcode,
				"country" => $country,
				"website" => $website,
				"address1" => $address1,
				"address2" => $address2,
				"latitude" => $latitude,
				"longtitude" => $longtitude,
				"contact" => $contact,
				"vauthorizeid" => $authorizeid,
				"vauthorizekey" => $authorizekey,
				"date_updated" => date('Y-m-d H:i:s')
			));

			if($updatevenue->save()) {
				echo json_encode(array("result"=>"Successful!"));
			} else {
					$errors = array();
					foreach($updatevenue->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
				echo json_encode(array("result"=>$errors));
			}
		}
	}

	public function BEworkshopCreatetitleAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			// $title = $request->getPost("title");
			// $titleslugs = strtolower(preg_replace('/[^\w#& ]/', '', $title));
			// $titleslugs = preg_replace('/\s+/', ' ', $titleslugs);
			// $titleslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $titleslugs);
			$guid = new \Utilities\Guid\Guid();

			$createtitle = new Workshoptitle();
			$createtitle->assign(array(
				"workshoptitleid" => $guid->GUID(),
				"title" => $_POST['title'],
				"titleslugs" => $_POST['titleslugs'],
				"date_created" => date('Y-m-d'),
				"date_updated" => date('Y-m-d H:i:s')
			));
			if($createtitle->create()) {
				echo json_encode(array("result" => "Successful!"));
			} else {
				$errors = array();
				foreach($createtitle->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array("result" => $errors));
			}
		}
	}

	public function BEworkshopListoftitlesAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT * FROM workshoptitle LIMIT $offsetfinal, $num");
			$stmt->execute();
			$titles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$titlescount = Workshoptitle::find();
			$totalNumberOfTitles = count($titlescount);

		} else {
			$stmt = $db->prepare("SELECT * FROM workshoptitle WHERE title LIKE '%$keyword%' LIMIT $offsetfinal, $num");
			$stmt->execute();
			$titles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshoptitle WHERE title LIKE '%$keyword%' ");
			$stmt->execute();
			$titlescount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfTitles = count($titlescount);
		}

		echo json_encode(array('data' => $titles, 'index' => $page, 'total_items' => $totalNumberOfTitles));

	}

	public function BEworkshopremovetitleAction($titleid) {
		$title = Workshoptitle::findFirst("workshoptitleid = '$titleid' ");
		if($title) {
			if($title->delete()) {
				echo json_encode(array("result"=>"Successful!"));
			} else {
				echo json_encode(array("result"=>"Error!"));
			}
		} else {
			echo json_encode(array("result"=>"Error!"));
		}
	}

	public function BEworkshopChecktitlenameAction($titleslugs) {

		// $titleslugs = strtolower(preg_replace('/[^\w#& ]/', '', $titlename));
		// $titleslugs = preg_replace('/\s+/', ' ', $titleslugs);
		// $titleslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $titleslugs);

		$titlename = Workshoptitle::findFirst(" titleslugs = '$titleslugs' ");
		if($titlename) {
			echo json_encode(array("exists" => true));
		} else {
			echo json_encode(array("exists" => false));
		}
	}

	public function BEworkshopListAction() {
		$db = \Phalcon\DI::getDefault()->get('db');

		$titles = Workshoptitle::find();
		$venues = Workshopvenue::find();

        $stmt = $db->prepare("SELECT * FROM center ORDER BY centerstate, centertitle ");
        $stmt->execute();
        $centers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // FRONTEND USE
        $states = States::find();

        $stmt = $db->prepare(" SELECT * FROM center centercity ORDER BY centercity ");
        $stmt->execute();
        $cities = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart");
        $stmt->execute();
        $workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT * FROM workshop WHERE datestart >= '$datenow'  ORDER BY datestart");
        $stmt->execute();
        $TOTALWORKSHOPS = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $month = ''; $year='';
        $month_year = array();
        foreach ($TOTALWORKSHOPS as $date) {
        	$date_started = date_create($date['datestart']);
        	$month_date_started = date_format($date_started, "F");
        	$year_date_started = date_format($date_started, "Y");
	        	//DATE DROPDOWN LIST 
        	if($month != $month_date_started && $year != $year_date_started) {
        		$month_year[] = array(
        			"datelike" => date_format($date_started, "Y-m"),
        			"month_year" => date_format($date_started, "F, Y")
        			);
        		$month = $month_date_started;
        	}
        }
	        	

		echo json_encode(array(
			"venues" => $venues->toArray(),
			"titles" => $titles->toArray(),
			"centers" => $centers,
			"states" => $states->toArray(),
			"cities" => $cities,
			"workshops" => $workshops,
			"date_choices" => $month_year
		));
	}

	public function BEworkshopCreateworkshopAction() {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$title = $request->getPost('title');
			$venue = $request->getPost('venue');
			$center = $request->getPost('center');
			$associatedcenter = $request->getPost('associatedcenter');
			$tuition = $request->getPost('tuition');
			$datestart = $request->getPost ('datestart');
			if(strlen($datestart) > 10) {
				$mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				$dates  = explode(" ", $datestart);
				$datestart   = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            } 
          
			$starthour = $request->getPost('starthour');
			$startminute = $request->getPost('startminute');
			$starttimeformat = $request->getPost('starttimeformat');
			$dateend = $request->getPost('dateend');
			if(strlen($dateend) > 10) {
				$mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				$dates = explode(" ", $dateend);
				$dateend = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
			}
			$endhour = $request->getPost('endhour');
			$endminute = $request->getPost('endminute');
			$endtimeformat = $request->getPost('endtimeformat');
			$about = $request->getPost('about');
			$note = $request->getPost('note');
			$accomodation = $request->getPost('accomodation');
			$miscellaneous = $request->getPost('miscellaneous');
			$metatitle = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatitle'));
			$metadesc = $request->getPost('metadesc');

			$guid = new \Utilities\Guid\Guid();
			$workshopid = $guid->GUID();

			$workshop = new Workshop();
			$workshop->assign(array(
				"workshopid" => $workshopid,
				"title" => $title,
				"venue" => $venue,
				"center" => $center,
				"tuition" => $tuition,
				"datestart" => $datestart,
				"starthour" => $starthour,
				"startminute" => $startminute,
				"starttimeformat" => $starttimeformat,
				"dateend" => $dateend,
				"endhour" => $endhour,
				"endminute" => $endminute,
				"endtimeformat" => $endtimeformat,
				"about" => $about,
				"note" => $note,
				"accomodation" => $accomodation,
				"miscellaneous" => $miscellaneous,
				"metatitle" => $metatitle,
				"metadesc" => $metadesc,
				"date_created" => date('Y-m-d'),
				"date_updated" => date('Y-m-d H:i:s')
			));
			if($workshop->create()) {
				foreach($associatedcenter as $centerid){
					$workshopassociatedcenter = new Workshopassociatedcenter();
					$workshopassociatedcenter->workshopid = $workshopid;
					$workshopassociatedcenter->centerid = $centerid;
					if($workshopassociatedcenter->save()) {
						// echo json_encode(array("result" => "Successful!"));
					}
				}

				echo json_encode(array("result" => "Successful!"));
			} else {
				$errors = array();
				foreach($workshop->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array("result" => $errors));
			}

		}
	}

	public function BEworkshopListofworkshopAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT center.centertitle, workshop.*, workshopvenue.venuename, workshoptitle.title as titlename FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid  LIMIT $offsetfinal, $num");
			$stmt->execute();
			$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$workshopscount = Workshop::find();
			$totalNumberOfWorkshops = count($workshopscount);

		} else {
			$stmt = $db->prepare("SELECT center.centertitle,workshop.*, workshopvenue.venuename, workshoptitle.title as titlename FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.title LIKE '%$keyword%' LIMIT $offsetfinal, $num");
			$stmt->execute();
			$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshop WHERE title LIKE '%$keyword%' ");
			$stmt->execute();
			$workshopscount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfWorkshops = count($workshopscount);
		}
		echo json_encode(array('data' => $workshops, 'index' => $page, 'total_items' => $totalNumberOfWorkshops, "counter" => $offsetfinal));
	}

	public function BEworkshopRemoveworkshopAction($workshopid) {
		$workshopX = Workshop::findFirst(" workshopid = '$workshopid' ");
		if($workshopX) {
			if($workshopX->delete()) {
				echo json_encode(array("result" => "Successful!"));
			} else {
				echo json_encode(array("result" => "Error!"));
			}
		}
	}

	public function BEworkshopEditworkshopAction($workshopid) {
		
		$workshopI = Workshop::findFirst("workshopid = '$workshopid' ");
		$workshopA = Workshopassociatedcenter::find("workshopid = '$workshopid' ");
		$newdata = array();
		foreach ($workshopA as $workshopA) {
			// $newdata = $workshopA->centerid; 
			array_push($newdata,$workshopA->centerid);
		}
		$data = array(
			"workshopid" => $workshopI->workshopid,
			"title"=> $workshopI->title,
			"venue"=> $workshopI->venue,
			"center"=> $workshopI->center,
			"tuition"=> $workshopI->tuition,
			"sale"=> $workshopI->sale,
			"datestart"=> $workshopI->datestart,
			"starthour"=> $workshopI->starthour,
			"startminute"=> $workshopI->startminute,
			"starttimeformat"=> $workshopI->starttimeformat,
			"dateend"=> $workshopI->dateend,
			"endhour"=> $workshopI->endhour,
			"endminute"=> $workshopI->endminute,
			"endtimeformat"=> $workshopI->endtimeformat,
			"about"=> $workshopI->about,
			"note"=> $workshopI->note,
			"accomodation"=> $workshopI->accomodation,
			"miscellaneous"=> $workshopI->miscellaneous,
			"metatitle"=> $workshopI->metatitle,
			"metadesc"=> $workshopI->metadesc,
			"date_created"=>$workshopI->date_created,
			"date_updated"=> $workshopI->date_updated,
			"id"=> $workshopI->id,
			"centerid"=> $workshopI->workshopid,
			"associatedcenter"=> $newdata
		 );
		echo json_encode(array(
			"workshopprop" => $data
		));
	}

	public function BEworkshopUpdateworkshopAction($workshopid) {
		$request = new \Phalcon\Http\Request();
		if($request->isPost()){
			$title = $request->getPost('title');
			$venue = $request->getPost('venue');
			$center = $request->getPost('center');
			$tuition = $request->getPost('tuition');
			$associatedcenter = $request->getPost('associatedcenter');
			$sale = $request->getPost('sale');
			$datestart = $request->getPost ('datestart');

			if(strlen($datestart) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $datestart);
                 $datestart = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }
				// $mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				// $dates  = explode(" ", $datestart);
				// $datestart   = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
			$starthour = $request->getPost('starthour');
			$startminute = $request->getPost('startminute');
			$starttimeformat = $request->getPost('starttimeformat');
			$dateend = $request->getPost('dateend');
			if(strlen($dateend) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $dateend);
                 $dateend = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }
				// $dates = explode(" ", $dateend);
				// $dateend = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
			$endhour = $request->getPost('endhour');
			$endminute = $request->getPost('endminute');
			$endtimeformat = $request->getPost('endtimeformat');
			$about = $request->getPost('about');
			$note = $request->getPost('note');
			$accomodation = $request->getPost('accomodation');
			$miscellaneous = $request->getPost('miscellaneous');
			$metatitle = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatitle'));
			$metadesc = $request->getPost('metadesc');

			$upworkshop = Workshop::findFirst(" workshopid = '$workshopid' ");
			$upworkshop->assign(array(
				"title" => $title,
				"venue" => $venue,
				"center" => $center,
				"tuition" => $tuition,
				"sale" => $sale,
				"datestart" => $datestart,
				"starthour" => $starthour,
				"startminute" => $startminute,
				"starttimeformat" => $starttimeformat,
				"dateend" => $dateend,
				"endhour" => $endhour,
				"endminute" => $endminute,
				"endtimeformat" => $endtimeformat,
				"about" => $about,
				"note" => $note,
				"accomodation" => $accomodation,
				"miscellaneous" => $miscellaneous,
				"metatitle" => $metatitle,
				"metadesc" => $metadesc,
				"date_updated" => date('Y-m-d H:i:s')
			));
			if($upworkshop->save()) {

				$remove = Workshopassociatedcenter::find("workshopid = '$workshopid' ");
				if($remove) {
					if($remove->delete()) {
						
					} else {
						
					}
				}

				foreach($associatedcenter as $centerid){
					$workshopassociatedcenter = new Workshopassociatedcenter();
					$workshopassociatedcenter->workshopid = $workshopid;
					$workshopassociatedcenter->centerid = $centerid;
					if($workshopassociatedcenter->save()) {
						// echo json_encode(array("result" => "Successful!"));
					}
				}
				echo json_encode(array("result"=> "Successful!"));
			} else {
				$errors = array();
				foreach($upworkshop->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array("result" => $errors));
			}
		}
	}

	public function BEworkshopUpdatetitleAction() {
		$uptitle = Workshoptitle:: findFirst(" workshoptitleid = '$_POST[titleid]' ");
		if($uptitle) {
			$uptitle->title = $_POST['title'];
			$uptitle->titleslugs = $_POST['titleslugs'];
			$uptitle->date_updated = date('Y-m-d H:i:s');
			if($uptitle->save()) {
				echo json_encode(array("result"=> "Successful!"));
			} else {
				$errors = array();
				foreach($uptitle->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array("result" => $errors));
			}
		}
	}

	public function loadcityViastatecodeAction($statecode) {
		if($statecode == 'All' || $statecode == '') {
			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare(" SELECT * FROM center GROUP BY centercity ORDER BY centercity ");
			$stmt->execute();
			$centercities = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare(" SELECT * FROM center WHERE centerstate = '$statecode' GROUP BY centercity ORDER BY centercity ");
			$stmt->execute();
			$centercities = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}
		
		echo json_encode(array("cities" => $centercities));
	}

	public function workshopschedulePaginationAction($titleslugs, $offset) {
			$db = \Phalcon\DI::getDefault()->get('db');
			$finaloffset = ($offset * 10) - 10;	

			$request = new \Phalcon\Http\Request();
			if($request->isPost()) {
				$state = $request->getPost('state');
			}

		if($titleslugs == 'all') {
	        $stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart LIMIT $finaloffset, 10");
        	$stmt->execute();
        	$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        	$totalWorkshops = Workshop::find();
		} else {
			$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
        	$stmt->execute();
        	$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        	$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart");
        	$stmt->execute();
        	$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}

		echo json_encode(array(
			"workshops" => $workshops,
			"totalworkshops" => count($totalWorkshops),
			"finaloffset" => $finaloffset,
			"state" => $state
		));	
	}

	public function workshopschedulepaginationangularAction() {
		$db = \Phalcon\DI::getDefault()->get('db');
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$titleslugs = $request->getPost('titleslugs');
			$title = $request->getPost('title');
			$offset = $request->getPost('offset');

			$finaloffset = ($offset * 10) - 10;

			if($titleslugs == 'all') {
				$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$totalWorkshops = Workshop::find();
			} else {
				$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}

			echo json_encode(array(
				"titleslugs" => $titleslugs,
				"offset" => $offset,
				"workshops" => $workshops,
				"totalworkshops" => count($totalWorkshops)
			));
		}
	}

	public function workshopSchedulelistAction($titleslugs, $offset) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$datenow = date('Y-m-d');
		$finaloffset = ($offset * 10) - 10;

		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$title = $request->getPost('title');
			if($title == 'all') { $title = ''; }

			$state = $request->getPost('state');
			if($state == 'All') { $state = ''; }

			$city = $request->getPost('city');
			if($city == false) { $city = ''; }
		
			$datechoice = $request->getPost('datechoice');
		}

		if($titleslugs == 'all') {
			if(($title == null || $title == '') && ($state == '' || $state == null ) && ($city == null || $city == '') && ($datechoice == null || $datechoice == '')) {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "1";
			} else {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%')  ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "2";
			}

		} else {
			if(($title === null) && ($state == '' || $state == null ) && ($city == null || $city == 'false') && $datechoice == null) {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshoptitle.titleslugs = '$titleslugs' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid  WHERE workshoptitle.titleslugs = '$titleslugs' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "3";
			} 
			else {
				if($title == '') {
					$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshoptitle.titleslugs LIKE '%$title%' AND workshop.datestart >= '$datenow' AND (center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart LIMIT $finaloffset, 10");
					$stmt->execute();
					$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.titleslugs LIKE '%$title%' AND workshop.datestart >= '$datenow' AND (center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
					$stmt->execute();
					$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
					$wew = "4";
				} else {
					$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') or workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND workshopvenue.SPR LIKE '%$state%' AND workshopvenue.city LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart LIMIT $finaloffset, 10");
					$stmt->execute();
					$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					$stmt = $db->prepare("SELECT workshop.workshopid FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') or workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND workshopvenue.SPR LIKE '%$state%' AND workshopvenue.city LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
					$stmt->execute();
					$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
					$wew = "5";
				} 

			}
		}

		$date_s = array(); 
		foreach($workshops as $date) {
			$date_started = date_create($date['datestart']);
			$new_date_started = date_format($date_started, "M Y");
			$month_date_started = date_format($date_started, "F");
			$year_date_started = date_format($date_started, "Y");

			$date_ended = date_create($date['dateend']);
			$new_date_ended = date_format($date_ended, "M Y");
			$year_date_ended = date_format($date_ended, "Y");

	        	if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
		        	if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
		        		$new_date_started = date_format($date_started, "M d-");
		        		$new_date_ended = date_format($date_ended,"d, Y");
		        		$date_s[] = $new_date_started . $new_date_ended;
		        	} else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
		        		$date_s[] = date_format($date_started, "M d-") . date_format($date_ended, "M d, Y");
		        	}
	        	} else { //IF YEAR IS !SAME
	        		$date_s[] = date_format($date_started, "M d, Y - ") . date_format($date_ended, "M d, Y");
	        	}
	        }
	        
	        //pwede na itong tanggalin pag okay na :D 
	        $filter = array("title"=>$title, "state" => $state, "city" => $city, "datechoice" => $datechoice, "sql" => $wew); 

	        $lastnumber = $finaloffset + count($workshops);

	        echo json_encode(array(
	        	"filter" => $filter,
	        	"titleslugs" => $titleslugs,
	        	"index" => $offset,
	        	"lastnumber" => $lastnumber,
	        	"workshops" => $workshops,
	        	"total_items" => count($totalWorkshops),
	        	"dates_of_schedule" => $date_s,
	        	"counter" => $finaloffset,
	        ));
	}

	public function workshopdetailAction($workshopid) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$stmt = $db->prepare("SELECT workshop.*, center.*,centerphonenumber.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber on centerphonenumber.centerid=center.centerid WHERE workshop.workshopid = '$workshopid' ");
		$stmt->execute();
		$workshop = $stmt->fetch(\PDO::FETCH_ASSOC);

		echo json_encode(array(
			"workshopprop" => $workshop
		));
	}
	public function workshoplocationAction($workshopid) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN centerlocation ON workshop.center = centerlocation.centerid  WHERE workshop.workshopid = '$workshopid' ");
		$stmt->execute();
		$workshoplocation = $stmt->fetch(\PDO::FETCH_ASSOC);
		echo json_encode(array(
			"workshoplocation" => $workshoplocation
		));
	}
	public function registrantAction() {
		$db = \Phalcon\DI::getDefault()->get('db');
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$workshopid = $request->getPost('workshopid');
			$titlename = $request->getPost('titlename');
			$device = $request->getPost('device');
			$deviceos = $request->getPost('deviceos');
			$devicebrowser = $request->getPost('devicebrowser');

			$thisWorkshop = Workshop::findFirst(" workshopid = '$workshopid' ");
			if($thisWorkshop) {
				if($thisWorkshop->sale == null || $thisWorkshop->sale == 0) {
					$pay = $thisWorkshop->tuition;
				} else {
					$pay = $thisWorkshop->sale;
				}
				// if($thisWorkshop->datestart == $thisWorkshop->dateend) {
				// 	$when = date_create($thisWorkshop->datestart);
				// 	$when = date_format($when,"l, F d, Y");
				// }
				$timestart = $thisWorkshop->starthour .":". $thisWorkshop->startminute ." ". $thisWorkshop->starttimeformat;
				$timeend = $thisWorkshop->endhour .":". $thisWorkshop->endminute ." ". $thisWorkshop->endtimeformat;
				$datestart_created = date_create($thisWorkshop->datestart);
				$when = date_format($datestart_created,"l, F d, Y");
				$workshopdate = date_format($datestart_created,"m/d/Y");
				$workshopschedule = $workshopdate . ", ".$timestart." - ".$timeend; 
				$venueid = $thisWorkshop->venue;
				$centerid = $thisWorkshop->center;
			}
			$stmt = $db->prepare("SELECT * FROM center LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE center.centerid = '$centerid' ");
			$stmt->execute();
			$thisCenter = $stmt->fetch(\PDO::FETCH_ASSOC);
			// $centeremail = $thisCenter['email'];
			// $centertitle = $thisCenter['centertitle'];
			// $centerstate = $thisCenter['centerstate'];
			// $centerstate = $thisCenter['centerstate'];

			$thisVenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
			if($thisVenue){
				$venuename = $thisVenue->venuename;
				$venueaddress = $thisVenue->address1;
				$venuezipcode = $thisVenue->zipcode;
				$venuephone = $thisVenue->contact;
				$venuewww = $thisVenue->website;
				$venuelat = $thisVenue->latitude;
				$venuelon = $thisVenue->longtitude;
			}
			else{
				$venuename = '';
				$venueaddress = '';
				$venuezipcode = '';
				$venuephone = '';
				$venuewww = '';
				$venuelat = '';
				$venuelon = '';
			}


			$firstname = $request->getPost('firstname');
			$lastname = $request->getPost('lastname');
			$name = $request->getPost('firstname') ." ".$request->getPost('lastname');
			$address1 = $request->getPost('address1');
			$address2 = $request->getPost('address2');
			$country = $request->getPost('country');
			$city = $request->getPost('city');
			$state = $request->getPost('state');
			$zipcode = $request->getPost('zipcode');
			$contact = $request->getPost('contact');
			$email = $request->getPost('email');
			$authorizeid = $request->getPost('authorizeid');
			$authorizekey = $request->getPost('authorizekey');
			// $creditcard = $request->getPost('creditcard');
			$creditcardnumber = $request->getPost('creditcardnumber');
			$expimonth = $request->getPost('expimonth');
			$expiyear = $request->getPost('expiyear');
				$expiyear= substr($expiyear,2);
			$securitycode = $request->getPost('securitycode');

			// $post_url = "https://test.authorize.net/gateway/transact.dll";
			$post_url = "https://secure.authorize.net/gateway/transact.dll";
            $post_values = array(
                "x_login"           => $authorizeid,
                "x_tran_key"        => $authorizekey,
                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_type"            => "AUTH_CAPTURE",
                "x_method"          => "CC",
                "x_card_num"        => $creditcardnumber,
                "x_exp_date"        => $expimonth.$expiyear,
                "x_amount"          => $pay,
                "x_description"     => $titlename,
                "x_first_name"      => $firstname,
                "x_last_name"       => $lastname,
                "x_address"         => $address1,
                "x_state"           => $state,
                "x_zip"             => $zipcode,
                "x_card_code"		=> $securitycode
                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

			$post_string = "";
            foreach( $post_values as $key => $value )
                { $post_string .= "$key=" . urlencode( $value ) . "&"; }
            $post_string = rtrim( $post_string, "& " );

            $request = curl_init($post_url); // initiate curl object
                curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
                curl_setopt($request, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
                curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
                $post_response = curl_exec($request); // execute curl post and store results in $post_response
                // additional options may be required depending upon your server configuration
                // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            $response_array = explode($post_values["x_delim_char"],$post_response);
 			
 			$paymentinfo = array(
 				'paymenttype' => $response_array[51],
				'confirmation' => $response_array[6],
				'securitycode' => $securitycode,
				'payment' => $pay
 			);

 			$registrantinfo = array(
				'name' => ucwords($name),
				'contact' => $contact,
				'address' => $address1,
				'country' => $country,
				'city' => $city,
				'state' => $state,
				'zipcode' => $zipcode
			);

			$workshopinfo = array(
				'titlename' => $titlename,
				'venue' => $venuename,
				'address' => $venueaddress,
				'zipcode' => $venuezipcode,
				'phone' => $venuephone,
				'website' => $venuewww,
				'latitude' => $venuelat,
				'longtitude' => $venuelon,
				'venueid' => $venueid
			);

            if($response_array[0] == 1) {


				$guid = new \Utilities\Guid\Guid();
				$workshopregistrantid = $guid->GUID();
				$notificationid  = $guid->GUID();
				$register = new Workshopregistrant();
				$register->assign(array(
					"workshopregistrantid" => $workshopregistrantid,
					"workshop" => $workshopid,
					"name" => ucwords($name),
					"address1" => ucwords($address1),
					"address2" => ucwords($address2),
					"country" => ucwords($country),
					"city" => ucwords($city),
					"state" => ucwords($state),
					"zipcode" => $zipcode,
					"email" => $email,
					"contact" => $contact,
					"dateregistered" => date('Y-m-d H:i:s'),
					"pay" => $pay,
					"paymenttype" => $response_array[51],
					"expimonth" => $expimonth,
					"expiyear" => $expiyear,
					"confirmation" => $response_array[6],
					"device" => $device,
					"deviceos" => $deviceos,
					"devicebrowser" => $devicebrowser,
					"date_created" => date('Y-m-d'),
					"date_updated" => date('Y-m-d H:i:s')
				));
				if($register->create()) {


					$notification = new Notification();
                        $notification->assign(array(
                            'notificationid' => $notificationid,
                            'centerid' => '',
                            'itemid' => $workshopregistrantid,
                            'name' => $name,
                            'datecreated' => date('Y-m-d H:i:s'),
                            'adminstatus' => 0,
                            'regionstatus' => 0,
                            'centerstatus' => 0,
                            'type' => 'workshop'
                            ));
                        if ($notification->save()) {

                        } 


					echo json_encode(array(
						"result" => "Successful!", 
						"authmes" => $response_array[3], 
						"device" => $device,
						"deviceos" => $deviceos,
						"devicebrowser" => $devicebrowser,
						"status" => $response_array[0],
						"paymentinfo" => $paymentinfo,
						"registrantinfo" => $registrantinfo,
						"workshopinfo" => $workshopinfo,
						'thisCenter' => $thisCenter
					));	
					$dc = new CB();

					$map = "http://maps.googleapis.com/maps/api/staticmap?center=".$venuelat.", ".$venuelon."&zoom=9&size=600x300&maptype=roadmap
&markers=color:red%7Clabel:C%7C".$venuelat.", ".$venuelon;
	                $body_registrant = '
	                <!DOCTYPE html>
	                <html>
	                <head>
	                <title></title>
	                <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
	                <style>
	                @media screen and (max-width:720px){
	                    .logo {
	                        float:none;
	                        display:block;
	                        width:100%;
	                        height:auto;
	                    }
	                }
	                </style>
	                </head>
	                <body style="font-family: "Droid Sans">
	                <div style="display:block; width:90%; margin:auto;">
						<div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
							<center><img src="http://bnb.gotitgenius.com/img/frontend/logo.gif"> <br>
							Phone : 1-877-477-YOGA <br>
							E-mail : customerservice@bodynbrain.com
							</center>
							<hr style="border: 0; height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
							<h1 style="color:#f15d22" align=center>Workshop Registrant Confirmation</h1>
							
							<div style="width:50%; border:1px solid #999; margin:auto; padding:2%;" class="letter">
							Dear '.ucwords(strtolower($name)).', <br>
					            <br>
					            Congratulations! You have made and appointment to try 
					            <b>"'.$titlename.'"</b> Workshop. <br>
					            Get Ready for less stress and more energy! <br>
					            We will contact you soon to confirm your appointment. <br>
					            Please be sure to arrive 15 minutes before the workshop. <br>
					            <br>
					            Please print this page for your records.
					            If you have any questions,please contact us at:
					            Phone: '.$venuephone.' or Visit our website: '.$venuewww.'
					            Your confirmation number is '.$response_array[6].' <br>
					            <br>
					            We look forward seeing you. Thank you!<br>
					            <br><br>
					            Truly yours,<br>
					            <br>
					            <b>The BodynBrain.com Team</b>
					        </div>
							
							<h2 style="background:#00adbb; padding:.5%; color:white">Registrant Information</h2>
							<h3 style="padding-left:20px;">Name</h3>
							<span style="padding-left:30px">'.ucwords(strtolower($name)).'</span>
						
							<h3 style="padding-left:20px;">Address</h3>
							<span style="padding-left:30px">'.$address1.'</span>
							
							<h2 style="background:#00adbb; padding:.5%; color:white">Payment Information</h2>
							<h3 style="padding-left:20px;">Creditcard number</h3>
							<span style="padding-left:30px">'.$creditcardnumber.'</span>
							
							<h3 style="padding-left:20px;">Confirmation number</h3>
							<span style="padding-left:30px">'.$response_array[6].'</span>
							
							<h3 style="padding-left:20px;">Payment</h3>
							<span style="padding-left:30px">Paid $'.$pay.'.00 via '.$response_array[51].'</span>

							<h2 style="background:#00adbb; padding:.5%; color:white">Workshop Information</h2>
							<h3 style="padding-left:20px;">What</h3>
							<span style="padding-left:30px">'.$titlename.'</span>
							
							<h3 style="padding-left:20px;">Address</h3>
							<span style="padding-left:30px">'.$venueaddress.'</span>
							
							<h3 style="padding-left:20px;">Phone</h3>
							<span style="padding-left:30px">'.$venuephone.'</span>
							
							<h3 style="padding-left:20px;">Map</h3>
							<br>
							<img src="'.$map.'" width="100%" height="auto">
						</div>
					</div>
	                </body>
	                </html>
	                ';
	                $send = $dc->sendMail($email,'BodynBrain: Workshop Confirmation Email',$body_registrant);

	                $body_center = '
	                <!DOCTYPE html>
	                <html>
	                <head>
	                <title></title>
	                <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
	                <style>
	                @media screen and (max-width:720px){
	                    .logo {
	                        float:none;
	                        display:block;
	                        width:100%;
	                        height:auto;
	                    }
	                }
	                </style>
	                </head>
				    <body style="font-family: "Droid Sans">
				    <div style="display:block; width:90%; margin:auto;">
						<div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
							<center>
								<img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
							</center>
						</div>
					    <div style="width:90%; border:1px solid #999; margin:auto; padding:2%;">
						Dear '.$thisCenter["centertitle"].' Center Manager, <br>
						<br>
						Congratulations. You\'ve just got a/an <b><i>'.$titlename.'</i></b> Sign-Up through BodynBrain.com. <br>
						<br>
						Please find the details below : <br>
						1. Name: '.ucwords(strtolower($name)).' <br>
						2. Phone: '.$contact.' <br>
						3. Email: '.$email.' <br>
						4. Workshop Schedule: '.$workshopschedule.' <br>
						5. Payment: Paid( '.$pay.' ) <br>
						<br>
						Please contact this person to confirm the appointment as soon as possible.  <br>
						<br>
						Thank you <br>
						<br>
						<b>BodynBrain.com</b><br>
						</div>
			        </div>
	               	</body>
	               	</html>
	                ';

	                // $subject_center = "BodynBrain: ".$titlename." Sign-Up for ".$centertitle.", ".$centerstate." Center";
	                // $send = $dc->sendMail($centeremail,$subject_center,$body_center);
	              
	                $DI = \Phalcon\DI::getDefault();
        			$app = $DI->get('application');
        			foreach($app->config->email as $bnbemail) {
        				$send = $dc->sendMail($bnbemail,$subject_center,$body_center);
        			}
				} else {
					$errors = array();
					foreach($register->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array(
						"result" => $errors, 
						"status" => $response_array,
						"number" => $creditcardnumber
					));
				}
			} else {
				echo json_encode(array(
					"authmes" => $response_array[3],
					"status" => $response_array,
					"number" => $creditcardnumber

				));
			}
		}
	}

	public function registrantslistAction($num, $page) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;

		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$title = $request->getPost('titlename');
			$venue = $request->getPost('venue');

			if ($title == '' &&  $venue == '') {
				$stmt = $db->prepare("SELECT workshopregistrant.*,workshopvenue.*, workshop.*, workshoptitle.title as 'titlename', center.centertitle as 'centervenue', workshopregistrant.contact as 'rcontact' from workshopregistrant LEFT JOIN workshop ON workshopregistrant.workshop = workshop.workshopid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid  ORDER BY workshopregistrant.dateregistered DESC LIMIT $offsetfinal, $num");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshopregistrant.*, workshop.*, workshoptitle.title as 'titlename', center.centertitle as 'venue' from workshopregistrant LEFT JOIN workshop ON workshopregistrant.workshop = workshop.workshopid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid");
				$stmt->execute();
				$workshopscount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$totalNumberOfWorkshops = count($workshopscount);
			} else {
				$stmt = $db->prepare("SELECT workshopregistrant.*,workshopvenue.*, workshop.*, workshoptitle.title as 'titlename', center.centertitle as 'centervenue', workshopregistrant.contact as 'rcontact' from workshopregistrant LEFT JOIN workshop ON workshopregistrant.workshop = workshop.workshopid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid WHERE workshoptitle.workshoptitleid LIKE '%$title%' AND center.centerid LIKE '%$venue%' ORDER BY workshopregistrant.dateregistered DESC LIMIT $offsetfinal, $num");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshopregistrant.*, workshop.*, workshoptitle.title as 'titlename', center.centertitle as 'venue' from workshopregistrant LEFT JOIN workshop ON workshopregistrant.workshop = workshop.workshopid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.workshoptitleid LIKE '%$title%' AND center.centerid LIKE '%$venue%' ");
				$stmt->execute();
				$workshopscount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$totalNumberOfWorkshops = count($workshopscount);
			}

			echo json_encode(array(
				'data' => $workshops, 
				'index' => $page, 
				'total_items' => $totalNumberOfWorkshops, 
				"counter" => $offsetfinal,
				'venue' => $venue,
				'title' => $title
				));
		}

	}

	public function removeRegistrantAction($registrantid) {
		$remove = Workshopregistrant::findFirst("workshopregistrantid = '$registrantid' ");
		if($remove) {
			if($remove->delete()) {
				echo json_encode(array("result" => "Successful!"));
			} else {
				echo json_encode(array("result" => "Error!"));
			}
		}
	}

	public function overviewAction() {
		$db = \Phalcon\DI::getDefault()->get('db');
		$datenow = date('Y-m-d');
		$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart LIMIT 3");
		$stmt->execute();
		$upcomingschedule = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		$date_s = array(); 
		foreach($upcomingschedule as $date) {
			$date_started = date_create($date['datestart']);
			$new_date_started = date_format($date_started, "M Y");
			$month_date_started = date_format($date_started, "F");
			$year_date_started = date_format($date_started, "Y");

			$date_ended = date_create($date['dateend']);
			$new_date_ended = date_format($date_ended, "M Y");
			$year_date_ended = date_format($date_ended, "Y");

        	if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
	        	if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
	        		$new_date_started = date_format($date_started, "M d-");
	        		$new_date_ended = date_format($date_ended,"d");
	        		$date_s[] = $new_date_started . $new_date_ended . " ".date_format($date_ended,"Y");
	        	} else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
	        		$date_s[] = date_format($date_started, "M d-") . date_format($date_ended, "M d")." ".date_format($date_ended,"Y");
	        	}
        	} else { //IF YEAR IS !SAME
        		$date_s[] = date_format($date_started, "M d, Y - "). " " . date_format($date_ended, "M d, Y");
        	}
	    }
	    echo json_encode(array('upcomingschedule' => $upcomingschedule, 'schedule' => $date_s));
	}

	public function relatedAction() {
		$db = \Phalcon\DI::getDefault()->get('db');

		$titles = Workshoptitle::find();

		$relatedstories = array();
		$relatedmainarticles = array();
		$relatedcenterarticles = array();
		foreach($titles as $title) {

			$stmt = $db->prepare("SELECT * FROM testimonies WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$related = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$relatedstories[] = $related;

			$stmt = $db->prepare("SELECT * FROM news WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$relatednews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$relatedmainarticles[] = $relatednews;

			$stmt = $db->prepare("SELECT * FROM centernews WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$relatedcenternews = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$relatedcenterarticles[] = $relatedcenternews;
		}

		echo json_encode(array(
			"titles" => $titles->toArray(),
			"relatedstories" => $relatedstories,
			"relatedmainarticles" => $relatedmainarticles,
			"relatedcenterarticles" => $relatedcenterarticles
		));
	}

	// public function deletetransactionAction(){
	// 	$transactionManager = new TransactionManager();

	// 	$transaction = $transactionManager->get();
	// 	$workshopvenue = Workshopvenue::find();
	// 	foreach ($workshopvenue as $workshopvenue) {
			
		
	// 		$workshopvenue->setTransaction($transaction);
	// 		if($workshopvenue->delete()) {
	// 			echo json_encode(array("result" => "Successfully Deleted!"));
	// 			$transaction->commit();
	// 		} else {
	// 			echo json_encode(array("result" => "There was an error!"));
	// 			$transaction->rollback('Cannot save service prices.');
	// 		}
	// 		// var_dump('expression');
	// 	}
	// }

}