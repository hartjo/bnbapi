<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\Forgotpassword as Forgotpasswords;
use \Models\Center as Center;
use \Models\Centerregion as Centerregion;
use \Models\Centerdistrict as Centerdistrict;
use \Controllers\ControllerBase as CB;

class UserController extends \Phalcon\Mvc\Controller {

    public function loginAction($username,$password) {
        $request = new \Phalcon\Http\Request();
        $or_email = $username;
        // $user = Users::findFirst('username="' .  $request->getPost('username').'" AND  password="'. sha1($request->getPost('password')).'" AND status=1');

         $user = Users::findFirst(' (username="'.$username.'" OR email="'.$or_email.'") AND  password="'. sha1($password).'" AND status=1');////CURL*******
        if($user) {
            $jwt = new \Security\Jwt\JWT();
            $payload = array(
                "id" => $user->id,
                "username" => $user->username,
                "lastname" => $user->last_name,
                "firstname" => $user->first_name,
                "userrole" => $user->task,
                "profile_pic_name" => $user->profile_pic_name,
                "exp" => time() + (60 * 60)); // time in the future
            $token = $jwt->encode($payload, $app->config->hashkey);
            // echo json_encode(array('data' => $token));

            echo json_encode(array('success'=>$payload)); ////CURL*******
            $auditid = new \Utilities\Guid\Guid();
            $auditid->GUID();
            $db = \Phalcon\DI::getDefault()->get('db');
            $sql = $db->prepare("INSERT INTO auditlog (auditlog.logid,auditlog.datetime,auditlog.userid,auditlog.module,auditlog.event,auditlog.title) VALUES ('".$auditid->GUID()."','".date("Y-m-d H:i:s")."','".$user->id."','User','Login','Login Success: ".$username."')");
            $sql->execute();
        }else{
            echo json_encode(array('error' => 'Username or Password is invalid.'));
        }

    }

    public function userExistAction($name) {
        if(!empty($name)) {
            $user = Users::findFirst('username="' . $name . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }
    public function emailExistAction($name)
    {
        if (!empty($name)) {
            $user = Users::findFirst('email="' . $name . '"');
            if ($user) {
                echo json_encode(array('exists' => true));
            } else {
                echo json_encode(array('exists' => false));
            }
        }
    }
    public function activationAction()
    {
        if (!empty($_POST['code'])) {
            $code = $_POST['code'];
            $user = Users::findFirst('activation_code="' . $code . '"');
            if (isset($user->activation_code)) {
                $user->activation_code = '';
                $user->status = 1;
                if($user->save()){
                    echo json_encode(array('success' => 'Your account have been fully activated. You can now login.'));
                }else{
                    echo json_encode(array('error' => 'Cannot update record.'));
                }
            } else {
                echo json_encode(array('error' => 'No activation code found.'));
            }
        }
    }
    public function registerUserAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $username   = $request->getPost('username');
            $email      = $request->getPost('email');
            $password   = $request->getPost('password');
            $userrole   = $request->getPost('userrole');
            $firstname  = $request->getPost('fname');
            $lastname   = $request->getPost('lname');
            
            $bday   = $request->getPost('bday');
            $mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates  = explode(" ", $bday);
            $bday   = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

            $gender     = $request->getPost('gender');
            $status     = $request->getPost('status');
            $contact    = $request->getPost('contact');
            $profile_pic_name   = $request->getPost('profile_pic_name');

            switch($userrole) {
                case "Region Manager": 
                    $roleid   = $request->getPost('region');
                break;
                case "Center Manager":
                    $roleid   = $request->getPost('center');
                break;
                default:
                    $roleid   = 0;
            }

            /* Register User*/
            $guid = new \Utilities\Guid\Guid();
            $userid = $guid->GUID();
            $usave = new Users();
            $usave->id              = $userid;
            $usave->username        = $username;
            $usave->email           = $email;
            $usave->password        = sha1($password);
            $usave->task            = $userrole;
            $usave->roleid          = $roleid;
            $usave->first_name      = $firstname;
            $usave->last_name       = $lastname;
            $usave->birthday        = $bday;
            $usave->gender          = $gender;
            $usave->status          = $status;
            $usave->contact         = $contact;
            $usave->profile_pic_name= $profile_pic_name;
            $usave->activation_code = $code = $guid->GUID();
            $usave->created_at      = date("Y-m-d H:i:s");
            $usave->updated_at      = date("Y-m-d H:i:s");

            if(!$usave->create()){
                $errors = array();
                foreach ($usave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            }else{
                echo json_encode(array('success' => 'You are now successfuly registered.'));
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Add", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Add User : ". $username ."" /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            }

            switch($userrole) {
                case "Region Manager": 
                    $regionid = $request->getPost('region');
                    $regionmanagers = Centerregion::findFirst("regionid = '$regionid'");
                    if($regionmanagers) {
                        $regionmanagers->regionmanager = $userid;
                        if(!$regionmanagers->save()) {
                            echo json_encode(array('result' => 'There is an error saving Region Manager!'));
                        } else {
                            echo json_encode(array('result' => 'Region Manager successfully save!'));
                            echo "YEAH";
                        }
                    }
                break;
                case "District Manager": 
                    $districtid = $request->getPost('district');
                    $districtmanager = Centerdistrict::findFirst("districtid = '$districtid'");
                    if($districtmanager) {
                        $districtmanager->districtmanager = $userid;
                        if(!$districtmanager->save()) {
                            echo json_encode(array('result' => 'There is an error saving District Manager'));
                        } else {
                            echo json_encode(array('result' => 'District Manager successfully save'));
                            echo "YEAH DISTRICT";
                        }
                    }
                break;
                case "Center Manager": 
                    $centerid = $request->getPost('center');
                    $centermanager = Center::findFirst("centerid = '$centerid'");
                    if($centermanager) {
                        $centermanager->centermanager = $userid;
                        if(!$centermanager->save()) {
                            echo json_encode(array('result' => 'There is an error saving District Manager'));
                        } else {
                            echo json_encode(array('result' => 'District Manager successfully save'));
                            echo "YEAH DISTRICT";
                        }
                    }
                break;
            }
            $this->view->pick("users/create");
        }else{
            echo json_encode(array('error' => 'No post data.'));
        }
    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }

    public function userListAction($num, $page, $keyword) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($page * 10) - 10;
        if ($keyword == 'null' || $keyword == 'undefined') {
               
               $stmt = $db->prepare("SELECT * FROM users ORDER BY username  LIMIT " . $offsetfinal . ",10");
               $stmt->execute();
               $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

               $searchresult1 = Users::find();
               $totalusers = count($searchresult1);
            } 
            else {
                 $stmt = $db->prepare("SELECT * FROM users WHERE username LIKE '%" . $keyword . "%' or first_name LIKE '%" . $keyword . "%' or last_name LIKE '%" . $keyword . "%' ORDER BY username LIMIT " . $offsetfinal . ",10");
                 $stmt->execute();
                 $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                 $stmt = $db->prepare("SELECT * FROM users WHERE username LIKE '%" . $keyword . "%' or first_name LIKE '%" . $keyword . "%' or last_name LIKE '%" . $keyword . "%' ");
                 $stmt->execute();
                 $searchresult1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                 $totalusers = count($searchresult1);
            }

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalusers));

    }

    public function deleteUserAction($id){
        $dlt = Users::findFirst('id="' . $id . '"');
        $userid = $dlt->id;
        $userrole = $dlt->task;
        $username = $dlt->username;

        if ($dlt) {
            if($dlt->delete()){
                $data = array('success' => 'Successful!');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
                    "event" => "Delete", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                    "title" => "Delete User : ". $username ."" /*// Maybe some info here (confuse) XD*/
                    ));
                //END Audit Log
            } else {
                $data = array('error' => 'Theres an error');
            }
        }

        if($userrole == 'Region Manager') {
            $region = Centerregion::findFirst(" regionmanager = '$userid' ");
            if($region) {
                $region->regionmanager = 0;
                if($region->save()){
                    $data = array('success' => 'Successful!');
                } else {
                    $data = array('error' => 'Theres an error');
                }
            }
        } elseif ($userrole == 'District Manager') {
            $district = Centerdistrict::findFirst(" districtmanager = '$userid' ");
            if($district) {
                $district->districtmanager = 0;
                if($district->save()){
                    $data = array('success' => 'Successful!');
                } else {
                    $data = array('error' => 'Theres an error');
                }
            }
        } elseif ($userrole == 'Center Manager') {
            $center = Center::findFirst(" centermanager = '$userid' ");
            if($center) {
                $center->centermanager = 0;
                if($center->save()){
                    $data = array('success' => 'Successful!');
                } else {
                    $data = array('error' => 'Theres an error');
                }
            }
        } 
    }

    public function userInfoction($id) {
        $getInfo = Users::findFirst('id="'. $id .'"');
        $region = Centerregion::findFirst(" regionmanager = '$id' ");
        $district = Centerdistrict::findFirst(" districtmanager = '$id' ");
        $center = Center::findFirst(" centermanager = '$id' ");

        $data = array(
            'id' =>  $getInfo->id,
            'username' =>  $getInfo->username,
            'email' =>  $getInfo->email,
            'task' =>  trim($getInfo->task),
            'roleid' => $getInfo->roleid,
            'fname' =>  $getInfo->first_name,
            'lname' =>  $getInfo->last_name,
            'bday' =>  $getInfo->birthday,
            'gender' => $getInfo->gender,
            'status' => $getInfo->status,
            'contact' => $getInfo->contact,
            'profile_pic_name' => $getInfo->profile_pic_name,
            'regionid' => $region->regionid,
            'districtid' => $district->districtid,
            'centerid' => $center->centerid
            );
        echo json_encode($data);
    }

    public function userUpdateAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            //VARIABLE
            $userid         = $request->getPost('id');
            $username   = $request->getPost('username');
            $email      = $request->getPost('email');
            $password   = $request->getPost('password');
            $userrole   = $request->getPost('task');
            $firstname  = $request->getPost('fname');
            $lastname   = $request->getPost('lname');
            $bday       = $request->getPost('bday');
            $mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates  = explode(" ", $bday);
            $bday   = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            $gender     = $request->getPost('gender');
            $status     = $request->getPost('status');
            $contact    = $request->getPost('contact');
            $profile_pic_name   = $request->getPost('profile_pic_name');
            $oldpassword= $request->getPost('oldpassword');
            $newpassword= $request->getPost('newpassword');
            $password_c = $request->getPost('password_c');
         
            switch($userrole) {
                case "Region Manager": 
                    $roleid = $request->getPost('region');
                break;
                case "Center Manager": 
                    $roleid = $request->getPost('center');
                break;
                default:
                    $roleid = 0;
            }
            //SAVE

             if($oldpassword != '' && sha1($newpassword) == sha1($password_c))
             {   
                $getInfo = Users::findFirst('id="'. $userid .'" and password="'. sha1($oldpassword).'"');
                if(strlen($newpassword)<5){
                    echo json_encode(array('success' => 'TOOSHORT'));
                }
                elseif($getInfo){
                    $usave = Users::findFirst('id="' . $userid . '"');
                    if($usave) {
                        $usave->username        = $username;
                        $usave->email           = $email;
                        $usave->task            = $userrole;
                        $usave->first_name      = $firstname;
                        $usave->last_name       = $lastname;
                        $usave->birthday        = $bday;
                        $usave->gender          = $gender;
                        $usave->status          = $status;
                        $usave->contact         = $contact;
                        $usave->profile_pic_name= $profile_pic_name;
                        $usave->password        = sha1($newpassword);

                        if(!$usave->save()){
                            $errors = array();
                            foreach ($usave->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                            }
                            echo json_encode(array('error' => $errors));
                        }else{
                            echo json_encode(array('success' => 'UPDATED'));
                        }
                    }
                }
                else {
                        echo json_encode(array('success' => 'OLDPASSWORDNOTMATCH'));
                    }
                }
                elseif($oldpassword != '' && sha1($newpassword) != sha1($password_c))
                {
                    echo json_encode(array('success' => 'CONFIRMPASSWORDNOTMATCH'));
                }
                else{
                    $usave = Users::findFirst('id="' . $userid . '"');
                        if($usave) {
                            $usave->username        = $username;
                            $usave->email           = $email;
                            $usave->task            = $userrole;
                            $usave->roleid          = $roleid;
                            $usave->first_name      = $firstname;
                            $usave->last_name       = $lastname;
                            $usave->birthday        = $bday;
                            $usave->gender          = $gender;
                            $usave->status          = $status;
                            $usave->contact         = $contact;
                            $usave->profile_pic_name= $profile_pic_name;

                            if(!$usave->save()){
                                $errors = array();
                                foreach ($usave->getMessages() as $message) {
                                    $errors[] = $message->getMessage();
                                }
                                echo json_encode(array('error' => $errors));
                            }else{
                                echo json_encode(array('success' => 'UPDATED'));
                                //START Log
                                $audit = new CB();
                                $audit->auditlog(array(
                                    "module" =>"User", /*//Examaple News, Create Center, Slider, Events etc...*/
                                    "event" => "Update", /*//Example ADD , EdIT , Delete ,View Details etc...*/
                                    "title" => "Update User : ". $username ."" /*// Maybe some info here (confuse) XD*/
                                    ));
                        //END Audit Log
                            }
                        }

                }

            switch($userrole) {
                case "Region Manager": 
                    $existing = Centerregion::findFirst("regionmanager = '$userid' ");
                    if($existing) {
                        $existing->regionmanager = '0';
                        if(!$existing->save()) {
                            echo json_encode('Theres an error!');
                        } else {
                            echo json_encode('Successful!');
                        }
                    }
                    $D_existing = Centerdistrict::findFirst("districtmanager = '$userid' ");
                    if($D_existing) {
                        $D_existing->districtmanager = '0';
                        if(!$D_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }

                    $C_existing = Center::findFirst("centermanager = '$userid' ");
                    if($C_existing) {
                        $C_existing->centermanager = '0';
                        if(!$C_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }

                    $regionid = $request->getPost('region');
                    $regionmanagers = Centerregion::findFirst("regionid = '$regionid'");
                    if($regionmanagers) {
                        $regionmanagers->regionmanager = $userid;
                        if(!$regionmanagers->save()) {
                            $data['msg'] = "There is an error saving Region Manager!";
                        } else {
                           echo json_encode('Successful!');
                        }
                    }

                break;
                case "District Manager": 
                    $existing = Centerdistrict::findFirst("districtmanager = '$userid' ");
                    if($existing) {
                        $existing->districtmanager = '0';
                        if(!$existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                           echo json_encode('Successful!');
                        }
                    }
                    $R_existing = Centerregion::findFirst("regionmanager = '$userid' ");
                    if($R_existing) {
                        $R_existing->regionmanager = '0';
                        if(!$R_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                           echo json_encode('Successful!');
                        }
                    }
                    $C_existing = Center::findFirst("centermanager = '$userid' ");
                    if($C_existing) {
                        $C_existing->centermanager = '0';
                        if(!$C_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                           echo json_encode('Successful!');
                        }
                    }

                    $districtid = $request->getPost('district');
                    $districtmanager = Centerdistrict::findFirst("districtid = '$districtid'");
                    if($districtmanager) {
                        $districtmanager->districtmanager = $userid;
                        if(!$districtmanager->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }
                break;
                case "Center Manager": 
                    $existing = Center::findFirst("centermanager = '$userid' ");
                    if($existing) {
                        $existing->centermanager = '0';
                        if(!$existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }
                    $R_existing = Centerregion::findFirst("regionmanager = '$userid' ");
                    if($R_existing) {
                        $R_existing->regionmanager = '0';
                        if(!$R_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                           echo json_encode('Successful!');
                        }
                    }

                    $D_existing = Centerdistrict::findFirst("districtmanager = '$userid' ");
                    if($D_existing) {
                        $D_existing->districtmanager = '0';
                        if(!$D_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }

                    $centerid = $request->getPost('center');
                    $centermanager = Center::findFirst("centerid = '$centerid'");
                    if($centermanager) {
                        $centermanager->centermanager = $userid;
                        if(!$centermanager->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }
                break;
                default:
                    $R_existing = Centerregion::findFirst("regionmanager = '$userid' ");
                    if($R_existing) {
                        $R_existing->regionmanager = '0';
                        if(!$R_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }

                    $D_existing = Centerdistrict::findFirst("districtmanager = '$userid' ");
                    if($D_existing) {
                        $D_existing->districtmanager = '0';
                        if(!$D_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }

                    $C_existing = Center::findFirst("centermanager = '$userid' ");
                    if($C_existing) {
                        $C_existing->centermanager = '0';
                        if(!$C_existing->save()) {
                            $data['msg'] = "There is an error";
                        } else {
                            echo json_encode('Successful!');
                        }
                    }
            }
        }
    }

    public function userSetStatusAction($userid,$userstatus) {
        $thisUser = Users::findfirst("id = '$userid'");
        $thisUser->status = $userstatus;
        if(!$thisUser->save()) {
            $error = array();
            foreach($thisUser->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('result' => $errors));
        } else {
            echo json_encode(array('result' => $userstatus));
        }
    }
    public function loginCenterAction($userid) {
        $users = Users::findFirst("id  = '".$userid."' ");
        $mycenter = Center::findFirst("centermanager='".$userid."'");
        echo json_encode(array('user' =>$users,'center'=>$mycenter));
    }

    public function availableRegDisCenListAction($userid) {
        // CREATE USER QUERY

        // one manager per region and center only
        // $regions = Centerregion::find(" regionmanager = '0' ORDER BY regionname ");
        // $districts = Centerdistrict::find( " districtmanager = '0' ORDER BY districtname ");
        // $centers = Center::find("centermanager = '0' ORDER BY centertitle ");

        // multiple manager per region and center
        $regions = Centerregion::find(array("order"=>"regionname"));
        $districts = Centerdistrict::find(array("order"=>"districtname"));
        $centers = Center::find(array("order"=>"centertitle"));

        // UPDATE USER QUERY
        $upd_regions = Centerregion::find("regionmanager != '' ORDER BY regionname ");
        $upd_districts = Centerdistrict::find( " districtmanager != '' OR districtmanager = '$userid' ORDER BY districtname ");
        $upd_centers = Center::find("centermanager != '' OR centermanager = '$userid' ORDER BY centertitle ");

        // $init_region = Centerregion::findFirst(" regionmanager = '$userid' ");
        
        echo json_encode(array(
          "regions" =>  $regions->toArray(),
          "districts" => $districts->toArray(),
          "centers" => $centers->toArray(),
          "upd_regions" =>  $upd_regions->toArray(),
          "upd_districts" => $upd_districts->toArray(),
          "upd_centers" => $upd_centers->toArray()
        ));
    }
    public function forgotpasswordAction($email) {
       $data = array();
       $NMSemail = $email;

       $SubsEmail = Users::findFirst("email='" . $NMSemail . "'");
       $username = $SubsEmail->username;
       if ($SubsEmail == true) 
       {
        $a = '';
        for ($i = 0; $i < 6; $i++) {
            $a .= mt_rand(0, 9);
        }
        $token = sha1($a);

        $forgotEmail = Forgotpasswords::findFirst("email='" . $NMSemail . "'");
        if ($forgotEmail == true) 
        {
            if ($forgotEmail->delete()) {

            }
        }

        $forgotpassword = new Forgotpasswords();
        $forgotpassword->assign(array(
            'email' => $NMSemail,
            'token' => $token,
            'date' => date('y-m-d')
            ));
        if (!$forgotpassword->save()) 
        {
            $data['msg'] = "Something went wrong saving the data, please try again.";

        } 
        else 
        {
                        //mail
           $dc = new CB();
           $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">You&#39;re receiving this e-mail because you requested a password reset for your account at BodynBrain.
           <br>
           <br>
           Please click the Reset Password link and choose a new password:
           <br>
           <a href="'.$GLOBALS["baseURL"].'/bnbadmin/forgotpassword/changepassword/'.$NMSemail.'/'.$token.'" target="_blank">Reset Password</a>
           <br>
           <br>
           <br></div>';

           $send = $dc->sendMail($NMSemail,'BodynBrain : Password Reset',$body);
           $data['msg'] = 'Password Reset has been sent to your Email '. $NMSemail;

                        //START Log
           /*$audit = new CB();
           $audit->auditlog(array(
            "module" =>"User", 
            "event" => "Forgot Password", 
            "title" => "User : ". $username ."" 
            ));*/
                        //END Audit Log


       }
   }
   else
   {
    $data = array('msg' => 'No account found with that email address.');

}
echo json_encode($data);
}

public function checktokenAction($email,$token) {
   $data = array();
   $forgottoken = Forgotpasswords::findFirst("token='" . $token . "' and email ='". $email ."'");
   if ($forgottoken == true) 
   {
    $data['msg'] = 'valid';
}
else
{
    $data['msg'] = 'invalid';
}
echo json_encode($data);
}

public function updatepasswordtokenAction() {

    $email = $_POST['email'];
    $token = $_POST['token'];
    $repassword = $_POST['repassword'];

    $data = array();
    $emailcheck = Users::findFirst("email='" . $email . "'");
    $username = $emailcheck->username;
    if ($emailcheck == true) 
    {
        $emailcheck->password = sha1($_POST['repassword']);
        if (!$emailcheck->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else 
        {

            $forgotEmail = Forgotpasswords::findFirst("token='" . $token . "'");
            if ($forgotEmail == true) 
            {
                if ($forgotEmail->delete()) {
                                 //START Log
                    /*$audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"User", 
                        "event" => "Forgot Password", 
                        "title" => "Password Updated User : ". $username ."" 
                        ));*/
                            //END Audit Log
                    }
                }
            $data['msg'] = "Password Change Success.";
            }
        }
        else
        {
        }
        echo json_encode($data); 
    }

    public function changepasswordAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $userid = $request->getPost('userid');
            $oldpassword = $request->getPost('oldpassword');
            $password = $request->getPost('password');
            $repassword = $request->getPost('repassword');

            $finduser = Users::findFirst('id = "'.$userid.'"');
            if($finduser){
                if($finduser->password == sha1($oldpassword)){
                    $finduser->password = sha1($password);
                    if($finduser->save()){
                        $data['type'] = 'success';
                        $data['msg'] = 'Password Successfuly Change';
                    }
                    else{
                        $data['type'] = 'danger';
                        $data['msg'] = 'Something went wrong please try again later';
                    }
                    
                }
                else{
                    $data['type'] = 'danger';
                    $data['msg'] = 'Old Password Not Match';
                }
                
            }

        }
        echo json_encode($data);
    }


}