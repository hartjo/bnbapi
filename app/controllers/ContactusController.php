<?php

namespace Controllers;

use \Models\Contactus as Contactus;
use \Models\Repliedmessage as Message;
use \Models\Notification as Notification;
use \Controllers\ControllerBase as CB;

class ContactusController extends \Phalcon\Mvc\Controller {
	    public function sendAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();
        $db = \Phalcon\DI::getDefault()->get('db');

                $id = $guid->GUID();
                $notificationid = $guid->GUID();
                $subject= $request->getPost('subject');
                $name= $request->getPost('name');
                $email= $request->getPost('email');
                $content= $request->getPost('content');
                $state= $request->getPost('state');
                $center= $request->getPost('center');

                $getcenter = $db->prepare("SELECT * FROM center LEFT JOIN centeremail on center.centerid=centeremail.centerid  WHERE center.centerid = '".$center."'");
                $getcenter->execute();
                $finalcenter = $getcenter->fetch(\PDO::FETCH_ASSOC);

                    $add = new Contactus();
                    $add->assign(array(
                        'id' => $id,
                        'name' => $name,
                        'email' => $email,
                        'state' => $state,
                        'city' =>  $finalcenter['centertitle'],
                        'subject' => $subject,
                        'content' => $content,
                        'datesubmitted' => date('Y-m-d H:i:s'),
                        'status' => 0
                        ));
                    // $add->save();
                    if (!$add->save()) {
                            $errors = array();
                            foreach ($add->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                             }
                            echo json_encode(array('error' => $errors));
                            $data['error'] ="!SAVE";
                        }

                    else{

                        $notification = new Notification();
                        $notification->assign(array(
                            'notificationid' => $notificationid,
                            'centerid' => '',
                            'itemid' => $id,
                            'name' => $name,
                            'datecreated' => date('Y-m-d H:i:s'),
                            'adminstatus' => 0,
                            'regionstatus' => 0,
                            'centerstatus' => 0,
                            'type' => 'message'
                            ));
                        if ($notification->save()) {

                        }

                        $dc = new CB();

                        $body0 = '
                        <style>
                         p {
                             white-space:pre-wrap;
                         }
                         div{
                            background-color: #eee;padding:20px;margin-bottom:10px;
                         }
                        </style>
                        <div>
                        ---------- Forwarded message ----------<br><br>
                        From: BodynBrain <customerservice@bodynbrain.com><br>
                        Date: '.date('Y-m-d').'<br>
                        Subject: '.$subject.'<br>
                        To: <a href="mailto:phil@bodynbrain.com">phil@bodynbrain.com</a>,

												<br><br><br>

                        My name: '.$name.'<br>
                        My email: '.$email.'<br><br>

                        State: '.$state.'<br>
                        Center: '.$finalcenter['centertitle'].'<br><br>

                        <p>'.nl2br($content).'</p></div>';

												$body1 = '
                        <style>
                         p {
                             white-space:pre-wrap;
                         }
                         div{
                            background-color: #eee;padding:20px;margin-bottom:10px;
                         }
                        </style>
                        <div>
                        ---------- Forwarded message ----------<br><br>
                        From: BodynBrain <customerservice@bodynbrain.com><br>
                        Date: '.date('Y-m-d').'<br>
                        Subject: '.$subject.'<br>
                        To: <a href="mailto:customerservice@bodynbrain.com">customerservice@bodynbrain.com</a>,
                        <a href="mailto:phil@bodynbrain.com">phil@bodynbrain.com</a>,

												<br><br><br>

                        My name: '.$name.'<br>
                        My email: '.$email.'<br><br>

                        State: '.$state.'<br>
                        Center: '.$finalcenter['centertitle'].'<br><br>

                        <p>'.nl2br($content).'</p></div>';

												if($center != undefined && $center != "" && $state != undefined && $state != "") {
													$send = $dc->sendMail2('phil@bodynbrain.com',$subject,$body0,$email);
													$send4 = $dc->sendMail2($finalcenter['email'],$subject,$body0,$email);
												} else {
													$send = $dc->sendMail2('phil@bodynbrain.com',$subject,$body,$email);
													$send2 = $dc->sendMail2('customerservice@bodynbrain.com',$subject,$body,$email);
												}

                        $data['success'] ="SAVE";
                             //START Log
                            /*$audit = new CB();
                            $audit->auditlog(array(
                                "module" =>"Appointment",
                                "event" => "Message",
                                "title" => "".$fname." ".$lname." Sent an Appointment in ".date('Y-m-d')."",
                                ));
							*/                            //END Audit Log
                        }
                     echo json_encode(array($data));
    }

    public function listcontactsAction($num, $page, $keyword ,$stat ) {
        $countnew = count(Contactus::find("status = 0 "));
        if ($keyword == 'undefined' && $stat=='undefined') {
            $contacts = Contactus::find(array("order" => "datesubmitted desc"));
        }
        else if($keyword == 'undefined' && $stat!='undefined'){
             $contacts = Contactus::find(array("status LIKE '%" . $stat . "%'","order" => "datesubmitted desc"));
        }
        else if($keyword != 'undefined' && $stat!='undefined'){
              $conditions = "email LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR
                  name LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR
                  subject LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%' OR
                  datesubmitted LIKE '%" . $keyword . "%' and status LIKE '%" . $stat . "%'";
             $contacts = Contactus::find(array($conditions,"order" => "datesubmitted desc"));
        }
        else {
            $conditions = "email LIKE '%" . $keyword . "%' OR
                          name LIKE '%" . $keyword . "%' OR
                          subject LIKE '%" . $keyword . "%' OR
                          datesubmitted LIKE '%" . $keyword . "%'";
            $contacts = Contactus::find(array($conditions,"order" => "datesubmitted desc"));
        }

        $currentPage = (int) ($page);

            // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $contacts,
                "limit" => 10,
                "page" => $currentPage
                )
            );

            // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'name' => $m->name,
                'email' => $m->email,
                'subject' => $m->subject,
                'content' => $content,
                'status' => $m->status,
                'datesubmitted' => $m->datesubmitted,
                'state' => $m->state,
                'city' => $m->city
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }

        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items,'countnew' => $countnew));
    }

    public function messagedeleteAction($id) {
        $data = $id;
        $request = Contactus::findFirst('id="'. $id.'"');
        $name = $request->name;
        $lname = $request->lname;
        // $data = array('error' => 'Not Found');
        if ($request) {
            if ($request->delete()) {
                $data = array('success' => 'REQUEST Deleted');
                $deletemessage = Message::find('rid="'.$id.'"');
                $deletemessage->delete();

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Contacts",
                    "event" => "Delete",
                    "title" => "Delete message from :". $name .""
                ));
            }
        }
        echo json_encode($data);
    }
    public function listreplyAction($id){
        $data = array();
        $getreply= Message::find('rid="' . $id . '" ');
        foreach ($getreply as $getreply) {
            $data[] = array(
                'id'=>$getreply->id,
                'rid'=>$getreply->rid,
                'message'=>$getreply->message,
                'date'=>$getreply->date
                );
        }
        echo json_encode($data);

    }
    public function viewreplyAction($id) {
        $data = array();
        $viewrequest = Contactus::findFirst('id="' . $id .'"');
        $fullname = $viewrequest->name;
        if ($viewrequest) {
            $viewrequest = array(
                'id' => $viewrequest->id,
                'name' =>$viewrequest->name,
                'email' =>$viewrequest->email,
                'content' =>$viewrequest->content,
                'state' =>$viewrequest->state,
                'city' =>$viewrequest->city,
                'status' => $viewrequest->status,
                'datesubmitted' =>$viewrequest->datesubmitted
                );

        $audit = new CB();
        $audit->auditlog(array(
            "module" =>"Contacts",
            "event" => "view",
            "title" => "view message from :".$fullname
            ));
        }
        echo json_encode($viewrequest);



        $read = Contactus::findFirst('id="' . $id .'"');
       /* $fname = $read->fname;
        $lname = $read->lname;*/

        if($read->status == 0){
            $read->status = 1;
            if (!$read->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
        }
    }
     public function replyrequestAction() {
        $data = array();

        $id = $_POST['rid'];
        $request = Contactus::findFirst('id="' . $id .'"');
        $fullname = $request->name;
      /*  $fname = $request->fname;
        $lname = $request->lname;*/
        $request->status = 2;
        if (!$request->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        }
        else {
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Contacts",
                "event" => "Reply",
                "title" => "Reply to a message from :".$fullname
                ));

                ///MAIL
                $dc = new CB();
                $body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">This is the reply to your message: "'.$_POST['content'].'"</div>' . '<br><br> Admin Reply: ' .$_POST['messages'];
                $send = $dc->sendMail($_POST['email'],'BodynBrain : Reply',$body);

                $repliedmessage = new Message();
                $repliedmessage->assign(array(
                        'rid'  => $id,
                        'message' => $_POST['messages'],
                        'date' => date('Y-m-d')
                        ));
                if (!$repliedmessage->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                }else{
                        $data['success'] = "Success";
                        //START Log
                        /*$audit = new CB();
                        $audit->auditlog(array(
                            "module" =>"Request Information",
                            "event" => "Reply",
                            "title" => "".$fname." ".$lname." Request Replied",
                            ));*/
                        //END Audit Log
                }
        }
        echo json_encode($data);
    }
}
