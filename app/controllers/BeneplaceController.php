<?php

namespace Controllers;


use \Models\News as News;
use \Models\Centercalendar as Centercalendar;
use \Models\Beneplace as Beneplace;
use \Models\Centerlocation as Centerlocation;
use \Models\Centeremail as Centeremail;
use \Models\Introsession as Introsession;
use \Models\Centernews as Centernews;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Centerpricingregclass as Centerpricingregclass;
use \Models\Centerpricingsessionfee as Centerpricingsessionfee;
use \Models\Centerhours as Centerhours;
use \Models\Centersociallinks as Centersociallinks;
use \Models\Center as Center;
use \Models\Centerimages as Centerimages;
use \Models\Centeroffer as Centeroffer;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Centersession1 as Centersession1;
use \Models\Centersession2 as Centersession2;
use \Models\Centersession3 as Centersession3;
use \Models\Centersession4 as Centersession4;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Models\Beneplacereferrallink as Beneplacereferrallink;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class BeneplaceController extends \Phalcon\Mvc\Controller {



  public function benePlaceAction(){




  }

  public function beneplacelistAction($num, $page, $keyword,$centerid){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid = center.centerid WHERE beneplace.centerid = '" .$centerid. "' ORDER BY beneplace.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid=center.centerid WHERE beneplace.centerid = '" .$centerid. "' ORDER BY beneplace.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } 
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid=center.centerid  WHERE beneplace.centerid = '" .$centerid. "' and beneplace.name LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.quantity LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.payment LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY beneplace.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM   beneplace LEFT JOIN center ON beneplace.centerid=center.centerid WHERE beneplace.centerid = '" .$centerid. "' and beneplace.name LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.quantity LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.email LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.payment LIKE '%" . $keyword . "%' or beneplace.centerid = '" .$centerid. "' and beneplace.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY beneplace.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }


    public function loadbeneplacesessionAction($sessionid,$userid){
        $usertype = Users:: findFirst('id="'.$userid.'"');
        $changestatus = Beneplace::findFirst("sessionid = '".$sessionid."'");
        if($changestatus){
            if($usertype->task == 'Administrator'){
                $changestatus->adminsessionstatus = 1;
            }
            else if(($usertype->task == 'Center Manager')){
                $changestatus->centersessionstatus = 1;
            }
            if (!$changestatus->save()) {
                $errors = array();
                foreach ($changestatus->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "status successfully saved!";
                $data['type'] = "success";

            }


        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM beneplace WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }

    public function allbeneplacelistAction($num, $page, $keyword){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid = center.centerid  ORDER BY beneplace.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid=center.centerid ORDER BY beneplace.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } 
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid=center.centerid  WHERE beneplace.name LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or beneplace.quantity LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or center.centertitle LIKE '%" . $keyword . "%' or beneplace.payment LIKE '%" . $keyword . "%' or beneplace.confirmationnumber LIKE '%" . $keyword . "%' or beneplace.program LIKE '%" . $keyword . "%' ORDER BY beneplace.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM   beneplace LEFT JOIN center ON beneplace.centerid=center.centerid WHERE beneplace.name LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or beneplace.quantity LIKE '%" . $keyword . "%' or beneplace.email LIKE '%" . $keyword . "%' or center.centertitle LIKE '%" . $keyword . "%' or beneplace.payment LIKE '%" . $keyword . "%' or beneplace.confirmationnumber LIKE '%" . $keyword . "%' or beneplace.program LIKE '%" . $keyword . "%' ORDER BY beneplace.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function addreferralAction(){
      $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){

            $linkid = $request->getPost('linkid');
            $title = $request->getPost('title');
            $link = $request->getPost('link');

            $findlink = Beneplacereferrallink::findFirst('linkid = "'.$linkid.'"');
            if($findlink){
              $findlink->title = $title;
              $findlink->link = $link;
              if($findlink->save()){
                $data['msg'] = "Referral link successfully saved!";
                $data['type'] = "success";
              }
              else{
                $data['msg'] = "Something went wrong please try again!";
                $data['type'] = "danger";
              }
            }
            else{
              $guid = new \Utilities\Guid\Guid();
              $linkid = $guid->GUID();
              $savelink = new Beneplacereferrallink();
              $savelink->linkid = $linkid;
              $savelink->title = $title;
              $savelink->link = $link;
              if($savelink->save()){
                $data['msg'] = "Referral link successfully saved!";
                $data['type'] = "success";
              }
              else{
                $data['msg'] = "Something went wrong please try again!";
                $data['type'] = "danger";
              }
            }
            


        }
        echo json_encode($data);
    }

    public function viewreferrallinkAction(){
      $findreferral = Beneplacereferrallink::findFirst();
      if($findreferral){
        echo json_encode($findreferral);
      }
    }



}