<?php

namespace Controllers;

use \Models\Auditlog as Auditlog;

class AuditlogController extends \Phalcon\Mvc\Controller {

	public function auditloglistAction($num, $page, $keyword){
		if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM auditlog LEFT JOIN users ON auditlog.userid=users.id  ORDER BY auditlog.datetime DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM auditlog LEFT JOIN users on auditlog.userid=users.id");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totallogs = count($searchresult1);
        } 
        else {

        	$offsetfinal = ($page * 10) - 10;

        	$db = \Phalcon\DI::getDefault()->get('db');
        	$stmt = $db->prepare("SELECT * FROM auditlog LEFT JOIN users ON auditlog.userid=users.id WHERE users.username LIKE '%".$keyword."%' or auditlog.module LIKE '%".$keyword."%' or auditlog.event LIKE '%".$keyword."%' or auditlog.title LIKE '%".$keyword."%' ORDER BY auditlog.datetime DESC LIMIT " . $offsetfinal . ",10");

        	$stmt->execute();
        	$searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        	$db1 = \Phalcon\DI::getDefault()->get('db');
        	$stmt1 = $db1->prepare("SELECT * FROM auditlog LEFT JOIN users on auditlog.userid=users.id WHERE users.username LIKE '%".$keyword."%' or auditlog.module LIKE '%".$keyword."%' or auditlog.event LIKE '%".$keyword."%' or auditlog.title LIKE '%".$keyword."%'");

        	$stmt1->execute();
        	$searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

        	$totallogs = count($searchresult1);

         
    
        }

      
        echo json_encode(array('loglist' => $searchresult, 'index' =>$page, 'total_items' => $totallogs));
	}
	
	
}
