<?php

namespace Controllers;

use \Models\Newsletterimage as Newsletterimage;
use \Models\Newsletter as Newsletter;
use \Models\News as News;
use \Models\Pdffiles as Pdffiles;
use \Models\Newsletterpdf as Newsletterpdf;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NewsletterController extends \Phalcon\Mvc\Controller {

    public function savenewsletterAction(){
        $request = new \Phalcon\Http\Request();
        $guid = new \Utilities\Guid\Guid();

                $id = $guid->GUID();
                $title= $request->getPost('title');
                $status= $request->getPost('status');
                /*    dto mouna daw itu     $featured= $request->getPost('featurednews');*/
                $thumbnail= $request->getPost('image');
                $datepublished= $request->getPost('datepublished');
                $pdffile= $request->getPost('pdffile');

                //date converter
                $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                $dates = explode(" ", $datepublished);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
                  


                if($pdffile == 1){
                     $pdf= $request->getPost('pdf');
                     $add = new Newsletterpdf();
                        $add->assign(array(
                        'id' => $id,
                        'title' => $title,
                        'status' => $status,
                        'pdf' =>$pdf,
                        'thumbnail' => $thumbnail,
                        'datepublished' => $d,
                        'type' =>'pdf',
                        'datecreated' => date('Y-m-d'),
                        'dateupdated' =>date('Y-m-d H:i:s')
                        ));

                }
                else{
                    $shortdesc= $request->getPost('newsdescription');
                    $content= $request->getPost('content');

                    $add = new Newsletter();
                    $add->assign(array(
                        'id' => $id,
                        'title' => $title,
                        'shortdesc' => $shortdesc,
                        'status' => $status,
                        /*'featured' => $featured,*/
                        'type' =>'html',
                        'thumbnail' => $thumbnail,
                        'content' => $content,
                        'datepublished' => $d,
                        'datecreated' => date('Y-m-d'),
                        'dateupdated' =>date('Y-m-d H:i:s')
                        ));
                    // $add->save();
                }

                if (!$add->save()) {
                    $errors = array();
                    foreach ($add->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                    $data['error'] ="!SAVE";
                } 

                else{
                    $data['success'] ="SAVE";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Newsletter", 
                        "event" => "Add", 
                        "title" => "Add Newsletter ".$title.""
                        ));
                }
                echo json_encode(array($data));
    }
     public function manageNewsAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("
            select * from 
            (
                select id as id, title as title, status as status, type as type, datepublished as datepublished from newsletter
                UNION
                select id as id, title as title, status as status, type as type, datepublished as datepublished  from newsletterpdf
                ) 
           as newsletter LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("
            select * from 
            (
               select id as id, title as title, status as status, type as type, datepublished as datepublished from newsletter
               UNION
               select id as id, title as title, status as status, type as type, datepublished as datepublished  from newsletterpdf
                ) 
           as newsletter");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalreportdirty = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("
            select * from 
            (
             select id as id, title as title, status as status, type as type, datepublished as datepublished from newsletter
             UNION
             select id as id, title as title, status as status, type as type, datepublished as datepublished  from newsletterpdf
                ) 
         as newsletter Where newsletter.title LIKE '%" . $keyword . "%' or newsletter.datepublished LIKE '%" . $keyword . "%' LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("
            select * from 
            (
                select id as id, title as title, status as status, type as type, datepublished as datepublished from newsletter
                UNION
                select id as id, title as title, status as status, type as type, datepublished as datepublished  from newsletterpdf
                ) 
         as newsletter Where newsletter.title LIKE '%" . $keyword . "%' or newsletter.datepublished LIKE '%" . $keyword . "%' ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalreportdirty = count($searchresult1);
            
        }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }


    public function checktitleAction($title){
        $titlecheck = Newsletter::find("title = '$title' ");
        $titlecheck1 = Newsletterpdf::find("title = '$title' ");

        $count = count($titlecheck) + count($titlecheck1);

        echo json_encode($count);
    }
     public function deletenewsAction($id) {
        $newsletter = Newsletter::findFirst('id="'. $id.'"');
        $title = $newsletter->title;
        if ($newsletter) {
            if ($newsletter->delete()) {
                $data[]=array('success' => "");   
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Newsletter", 
                    "event" => "Delete", 
                    "title" => "Delete Newsletter ".$title.""
                    ));
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }
     public function deletenewspdfAction($id) {
        $newsletter = Newsletterpdf::findFirst('id="'. $id.'"');
        $title = $newsletter->title;
        if ($newsletter) {
            if ($newsletter->delete()) {
                $data[]=array('success' => "");
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Newsletter", 
                    "event" => "Delete", 
                    "title" => "Delete Newsletter PDF ".$title.""
                    ));
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }
    public function changestatusAction($status,$id){
        $getInfo = Newsletter::findFirst('id="'. $id .'"');
        var_dump($getInfo);
        if($status == 1){
           $getInfo->status = 0;

           if(!$getInfo->save()){
                $data=array('error' => '!save');  
           }else{
                $data=array('success' => 'save');
           }
        }
        else{
           $getInfo->status = 1;
           $getInfo->save();
           $data=array('success' => 'Published');
        }
        echo json_encode($data);
    }
    public function changestatus1Action($status,$id){
        $getInfo = Newsletterpdf::findFirst('id="'. $id .'"');
        var_dump($getInfo);
        if($status == 1){
           $getInfo->status = 0;
           if(!$getInfo->save()){
                $data=array('error' => '!save');  
           }else{
                $data=array('success' => 'save');
           }
        }
        else{
           $getInfo->status = 1;
           $getInfo->save();
           $data=array('success' => 'Published');
        }
        echo json_encode($data);
    }
    //IMAGE UPLOAD
    public function saveimageAction() {

        $filename = $_POST['imgfilename'];

        $picture = new Newsletterimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
     
    }

    public function checkfileAction($filename){
        $file = Pdffiles::find("filename = '$filename' ");
        echo json_encode(count($file));
    }

    public function newslettereditoAction($id) {
        $data = array();
        $news = Newsletter::findFirst('id="' . $id . '"');
        if ($news) {
            $data = array(
                'id' => $news->id,
                'title' => $news->title,
                'status' => $news->status,
                'newsdescription' => $news->shortdesc,
                'image' => $news->thumbnail,
                'content' => $news->content,
                'datepublished' => $news->datepublished, 
                );
        }
        echo json_encode($data);
    }

    public function newspdfeditoAction($id) {
        $data = array();
        $news = Newsletterpdf::findFirst('id="' . $id . '"');
        if ($news) {
            $data = array(
                'id' => $news->id,
                'title' => $news->title,
                'status' => $news->status,
                'image' => $news->thumbnail,
                'pdf' => $news->pdf,
                'datepublished' => $news->datepublished, 
                );
        }
        echo json_encode($data);
    }


    public function updateNewsletterAction() {
        $data = array();
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){

            $id = $request->getPost('id');
            $title = $request->getPost('title');
            $status = $request->getPost('status');
            $image = $request->getPost('image');
            $newsdescription = $request->getPost('newsdescription');
            $content = $request->getPost('content');
            $datepublished = $request->getPost('hiddendate');

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
           
            if(strlen($datepublished)<=11){
                $d = $datepublished;
            }else{
                $dates = explode(" ", $datepublished);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }

            $Newsletter = Newsletter::findFirst('id="' . $id . '"');
            $Newsletter->assign(array(
                'title' => $title,
                'status' => $status,
                'shortdesc' => $newsdescription,
                'thumbnail' => $image,
                'content' => $content,
                'datepublished' => $d,
                'dateupdated' =>date('Y-m-d H:i:s')
                ));

            if (!$Newsletter->save()) {
                $errors = array();
                foreach ($Newsletter->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Newsletter", 
                    "event" => "Update", 
                    "title" => "Update Newsletter ".$title.""
                    ));
            
            }
        }
        echo json_encode($data);
    }

  public function updateNewspdfAction() {
        $data = array();
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){

            $id = $request->getPost('id');
            $title = $request->getPost('title');
            $status = $request->getPost('status');
            $image = $request->getPost('image');
            $pdf = $request->getPost('pdf');
            $datepublished = $request->getPost('hiddendate');

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');

            if(strlen($datepublished)<=11){
                $d = $datepublished;
            }else{
                $dates = explode(" ", $datepublished);
                $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }

            $Newsletter = Newsletterpdf::findFirst('id="' . $id . '"');
            $Newsletter->assign(array(
                'id' => $id,
                'title' => $title,
                'status' => $status,
                'thumbnail' => $image,
                'pdf' => $pdf,
                'datepublished' => $d,
                'dateupdated' =>date('Y-m-d H:i:s')
                ));

            if (!$Newsletter->save()) {
                $errors = array();
                foreach ($Newsletter->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['success'] = "Success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Newsletter", 
                    "event" => "Update", 
                    "title" => "Update Newsletter PDF ".$title.""
                    ));
            
            }
        }
        echo json_encode($data);
    }






    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Newsletterimage::findFirst('id="'. $imgid.'"');
        $filename = $img->filename;
        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");   
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listimageAction() {

        $getimages = Newsletterimage::find(array("order" => "id DESC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
        foreach ($getimages as $getimages) 
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        }
        echo json_encode($data);

    }

     public function savepdfAction() {

        $filename = $_POST['pdffilename'];

        $pdf = new Pdffiles();
        $pdf->assign(array(
            'filename' => $filename
            ));

        if (!$pdf->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
     
    }
    //IMAGE DELETE
    public function deletepdfAction($id) {
        $pdf = Pdffiles::findFirst('id="'. $id.'"');
        $filename = $pdf->filename;
        if ($pdf) {
            if ($pdf->delete()) {
                $data[]=array('success' => "");   
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }


    public function listpdfAction(){

        $getpdf = Pdffiles::find(array("order" => "id DESC"));
        if(count($getpdf) == 0){
            $data['error']=array('NOPDF');
        }else{
        foreach ($getpdf as $getpdf) 
        {
            $data[] = array(
                'id'=>$getpdf->id,
                'filename'=>$getpdf->filename
                );
        }
        }
        echo json_encode($data);
    }

    public function fenewsletterAction($num, $page){

        $newsletter = Newsletter::find(array("status = 1","order"=>"datepublished DESC"));
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $newsletter,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id'=>$m->id,
                'banner'=>$m->thumbnail,
                'title'=>$m->title,
                'date'=>$m->datepublished
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function fepdfnewsletterAction($num, $page){
        
        $pdfnewsletter = Newsletterpdf::find(array("status = 1","order"=>"datepublished DESC"));
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $pdfnewsletter,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                 'id'=>$m->id,
                'banner'=>$m->thumbnail,
                'title'=>$m->title,
                'date'=>$m->datepublished,
                'pdf'=>$m->pdf
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }




    public function loadStoryAction(){
        $stories = Testimonies::find("status = 1");
        foreach($stories as $stories) { 
            /*$slug = preg_replace("![^a-z0-9]+!i", "-", $story->subject);
            if($slug == $storyslugs ) { $thisSTORYid = $story->id; }*/
            $storya[] = array(
                'photo' =>$stories->photo,
                'subject' =>$stories->subject,
                'details'=>$stories->details,
                'author'=>$stories->author
                ); 
        }

        $mainnews = News::find(" status = 1 ORDER BY views DESC LIMIT 5 ");
        foreach($mainnews as $mainnews) { 
            $news[] = array(
                'banner'=>$mainnews->banner,
                'description'=>$mainnews->description
                );
        }
        echo json_encode(array(
            "storyprop" => $storya,
            "mainnews" => $news
        ));
    }
    public function fenewslettershowAction($id){
        $getinfo = Newsletter::findFirst('id = "'.$id.'"');
        if($getinfo){
            echo json_encode($getinfo->content);
        }
    }


}
