<?php

namespace Controllers;

use \Models\Introsession as Introsession;
use \Models\Center as Center;
use \Models\States as States;
use \Controllers\ControllerBase as CB;

class CenterincomeController extends \Phalcon\Mvc\Controller {

	public function oneononeAction() {
		  $thisMonth = date('Y') ."-".date('m')."-01";
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT 	introsession.datecreated,
      															center.centertitle,
      															center.centerstate,
      															SUM(introsession.paid) as 'centerincome',
                                    SUM(introsession.quantity) as 'totalquantity',
      															count(center.centerid) as 'noofregistrant' FROM introsession
      											LEFT JOIN center ON introsession.centerid = center.centerid
      											WHERE introsession.datecreated > '$thisMonth 00:00:00' AND center.centertype != 2
      											GROUP BY  YEAR(introsession.datecreated),
                                      MONTH(introsession.datecreated),
                                      DAY(introsession.datecreated),
      																introsession.centerid
      											");
      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $totalincome = 0;
        $totalregistrant = 0;
        $finalquantity = 0;
        foreach($sessions as $key => $value) {
        		$totalincome = $totalincome + $value['centerincome'];
        		$totalregistrant = $totalregistrant + $value['noofregistrant'];
            $finalquantity = $finalquantity + $value['totalquantity'];
        }

    $centerlist = Center::find(array("order"=>"centertitle"));
    $stateslist = States::find(array("order"=>"state"));

		echo json_encode(array(
      "thismonth" => $thisMonth,
			"sessions" => $sessions,
			"totalincome" => $totalincome,
			"totalregistrant" => $totalregistrant,
      "centers" => $centerlist->toArray(),
      "states" => $stateslist->toArray(),
      "finalquantity" => $finalquantity

		));
	}

  public function filteroneononeAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $center = $request->getPost('center');
      $state = $request->getPost('state');

      $yearfrom = $request->getPost('fromyear');
      $yearto = $request->getPost('toyear');
      $monthfrom = $request->getPost('frommonth');
      $monthto = $request->getPost('tomonth');
      $datefrom = $yearfrom."-".$monthfrom."-01";
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;

      if($center == 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      } elseif ($center != 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerid = '$center' AND center.centertype != 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } elseif ($center == 12345 && $state != "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centertype != 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } else {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
           WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centerid = '$center' AND center.centertype != 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      }

      $totalincome = 0;
      $totalregistrant = 0;
      $finalquantity = 0;
      foreach($sessions as $key => $value) {
        $totalincome = $totalincome + $value['centerincome'];
        $totalregistrant = $totalregistrant + $value['noofregistrant'];
        $finalquantity = $finalquantity + $value['totalquantity'];
      }

      echo json_encode(array(
        "finalquantity" => $finalquantity,
        "dateto" => $dateto,
        "datefrom" => $datefrom,
        "sessions" => $sessions,
        "totalincome" => $totalincome,
        "totalregistrant" => $totalregistrant,
      ));
    }
  }

  public function groupsessionAction() {
    $thisMonth = date('Y') ."-".date('m')."-01";
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
                                    center.centertitle,
                                    center.centerstate,
                                    SUM(groupclassintrosession.paid) as 'centerincome',
                                    SUM(groupclassintrosession.quantity) as 'totalquantity',
                                    count(groupclassintrosession.centerid) as 'noofregistrant' FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$thisMonth 00:00:00' AND center.centertype != 2
                            GROUP BY  YEAR(groupclassintrosession.datecreated),
                                      MONTH(groupclassintrosession.datecreated),
                                      DAY(groupclassintrosession.datecreated),
                                      groupclassintrosession.centerid
                            ");
      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $totalincome = 0;
        $totalregistrant = 0;
        $finalquantity = 0;
        foreach($sessions as $key => $value) {
            $totalincome = $totalincome + $value['centerincome'];
            $totalregistrant = $totalregistrant + $value['noofregistrant'];
            $finalquantity = $finalquantity + $value['totalquantity'];
        }

    $centerlist = Center::find(array("order"=>"centertitle"));
    $stateslist = States::find(array("order"=>"state"));

    echo json_encode(array(
      "thismonth" => $thisMonth,
      "sessions" => $sessions,
      "totalincome" => $totalincome,
      "totalregistrant" => $totalregistrant,
      "centers" => $centerlist->toArray(),
      "states" => $stateslist->toArray(),
      "finalquantity" => $finalquantity
    ));
  }

  public function filtergroupsessionAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $center = $request->getPost('center');
      $state = $request->getPost('state');

      $yearfrom = $request->getPost('fromyear');
      $yearto = $request->getPost('toyear');
      $monthfrom = $request->getPost('frommonth');
      $monthto = $request->getPost('tomonth');
      $datefrom = $yearfrom."-".$monthfrom."-01";
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;


      if($center == 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      } elseif ($center != 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerid = '$center' AND center.centertype != 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } elseif ($center == 12345 && $state != "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centertype != 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } else {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centerid = '$center' AND center.centertype != 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      }

      $totalincome = 0;
      $totalregistrant = 0;
      $finalquantity = 0;
      foreach($sessions as $key => $value) {
        $totalincome = $totalincome + $value['centerincome'];
        $totalregistrant = $totalregistrant + $value['noofregistrant'];
        $finalquantity = $finalquantity + $value['totalquantity'];
      }

      echo json_encode(array(
        "dateto" => $dateto,
        "datefrom" => $datefrom,
        "sessions" => $sessions,
        "totalincome" => $totalincome,
        "totalregistrant" => $totalregistrant,
        "finalquantity" => $finalquantity
      ));
    }
  }

  public function chartperdayAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $last10Days = date("Y-m-d", strtotime("-10 days"));

    $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                          FROM introsession
                          LEFT JOIN center ON introsession.centerid = center.centerid
                          WHERE introsession.datecreated > '$last10Days 00:00:00' AND center.centertype != 2
                          GROUP BY  YEAR(introsession.datecreated),
                                    MONTH(introsession.datecreated),
                                    DAY(introsession.datecreated)
                          ");
    $stmt->execute();
    $oneonone = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT center.centertype, groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                          FROM groupclassintrosession
                          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                          WHERE groupclassintrosession.datecreated > '$last10Days 00:00:00' AND center.centertype != 2
                          GROUP BY  YEAR(groupclassintrosession.datecreated),
                                    MONTH(groupclassintrosession.datecreated),
                                    DAY(groupclassintrosession.datecreated) "
                        );
    $stmt->execute();
    $groupclass = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                          FROM introsession
                          LEFT JOIN center ON introsession.centerid = center.centerid
                          WHERE introsession.datecreated > '$last10Days 00:00:00' AND center.centertype = 2
                          GROUP BY  YEAR(introsession.datecreated),
                                    MONTH(introsession.datecreated),
                                    DAY(introsession.datecreated)
                          ");
    $stmt->execute();
    $oneonone_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT center.centertype, groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                          FROM groupclassintrosession
                          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                          WHERE groupclassintrosession.datecreated > '$last10Days 00:00:00' AND center.centertype = 2
                          GROUP BY  YEAR(groupclassintrosession.datecreated),
                                    MONTH(groupclassintrosession.datecreated),
                                    DAY(groupclassintrosession.datecreated) "
                        );
    $stmt->execute();
    $groupclass_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $days = array();
    for($n=9; $n>=0; $n--) {
      $days[] = array(
        "categories" => date("M<\b\\r>d", strtotime("-".$n." days")),
        "fulldate" => date("Y-m-d", strtotime("-".$n." days")),
        "day" => date("d", strtotime("-".$n." days"))
      );
    }

    echo json_encode(array(
      "oneonone" => $oneonone,
      "groupclass" => $groupclass,
      "oneonone_f" => $oneonone_f,
      "groupclass_f" => $groupclass_f,
      "days" => $days
    ));
  }

  public function chartpermonthAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $last12Months = date("Y-m-d", strtotime("-11 months"));
    $initYear = date("Y", strtotime("-11 months"));
    $initMonth = date("m", strtotime("-11 months"));

    $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
      FROM introsession
      LEFT JOIN center ON introsession.centerid = center.centerid
      WHERE introsession.datecreated > '$last12Months 00:00:00' AND center.centertype != 2
      GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated)");
    $stmt->execute();
    $oneonone = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
      FROM groupclassintrosession
      LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
      WHERE groupclassintrosession.datecreated > '$last12Months 00:00:00' AND center.centertype != 2
      GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated) ");
    $stmt->execute();
    $groupclass = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
      FROM introsession
      LEFT JOIN center ON introsession.centerid = center.centerid
      WHERE introsession.datecreated > '$last12Months 00:00:00' AND center.centertype = 2
      GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated)");
    $stmt->execute();
    $oneonone_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $stmt = $db->prepare("SELECT groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
      FROM groupclassintrosession
      LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
      WHERE groupclassintrosession.datecreated > '$last12Months 00:00:00' AND center.centertype = 2
      GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated) ");
    $stmt->execute();
    $groupclass_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $months = array();
    for($m=11; $m>=0; $m--) {
      $months[] = array(
        "categories" => date("Y<\b\\r>M", strtotime("-".$m." months")),
        "fulldate" => date("Y-m-d", strtotime("-".$m." months")),
        "month" => date("m", strtotime("-".$m." months"))
      );
    }

    echo json_encode(array(
      "startdate" => $last12Months,
      "oneonone" => $oneonone,
      "groupclass" => $groupclass,
      "oneonone_f" => $oneonone_f,
      "groupclass_f" => $groupclass_f,
      "months" => $months,
      "inityear" => $initYear,
      "initmonth" => $initMonth
    ));

  }

  public function filterchartperdayAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $yearfrom = $request->getPost("fromyear");
      $monthfrom = $request->getPost("frommonth");
      $dayfrom = $request->getPost("fromday");
      $datefrom = $yearfrom."-".$monthfrom."-".$dayfrom;

      $yearto = $request->getPost("toyear");
      $monthto = $request->getPost("tomonth");
      $dayto = $request->getPost("today");
      $dateto = $yearto."-".$monthto."-".$dayto;

      //get the DAYS GAP between two dates
      $from=date_create($datefrom);
      $to=date_create($dateto);
      $fromdateformat = date_format($from,"M<\b\\r>d");
      $diff=date_diff($to,$from);
      $daysgap = $diff->format('%a');

      // $last10Days = date("Y-m-d", strtotime("-10 days"));

      $stmt = $db->prepare("SELECT center.centertype, introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                            FROM introsession
                            LEFT JOIN center ON introsession.centerid = center.centerid
                            WHERE introsession.datecreated > '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
                            GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated), DAY(introsession.datecreated) ");
      $stmt->execute();
      $oneonone = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT center.centertype, groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                            FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
                            GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated), DAY(groupclassintrosession.datecreated) ");
      $stmt->execute();
      $groupclass = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT center.centertype, introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                            FROM introsession
                            LEFT JOIN center ON introsession.centerid = center.centerid
                            WHERE introsession.datecreated > '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
                            GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated), DAY(introsession.datecreated) ");
      $stmt->execute();
      $oneonone_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT center.centertype, groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                            FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
                            GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated), DAY(groupclassintrosession.datecreated) ");
      $stmt->execute();
      $groupclass_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $days = array();
      for($n=0; $n<=$daysgap; $n++) {
        $days[] = array(
          "categories" => date("M<\b\\r>d", strtotime($datefrom." +".$n." days")),
          "fulldate" => date("Y-m-d", strtotime($datefrom." +".$n." days")),
          "day" => date("d", strtotime($datefrom." +".$n." days"))
        );
      }

      echo json_encode(array(
        "oneonone" => $oneonone,
        "groupclass" => $groupclass,
        "oneonone_f" => $oneonone_f,
        "groupclass_f" => $groupclass_f,
        "days" => $days,
      ));

    } // } end of request isPost
  }

  public function filterchartpermonthAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $yearfrom = $request->getPost("fromyear");
      $monthfrom = $request->getPost("frommonth");
      $datefrom = $yearfrom."-".$monthfrom."-01";

      $yearto = $request->getPost("toyear");
      $monthto = $request->getPost("tomonth");
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;

      $yearmonthto = $yearto."-".$monthto;
      $yearmonth = 0;
      $monthsgap = 0;
      //get the MONTH OCCURENCE between two dates
      while($yearmonthto != $yearmonth) {
        $yearmonth = date("Y-m", strtotime($datefrom." +".$monthsgap." months"));
        $monthsgap++;
      }

      $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                            FROM introsession
                            LEFT JOIN center ON introsession.centerid = center.centerid
                            WHERE introsession.datecreated > '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
                            GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated)");
      $stmt->execute();
      $oneonone = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT introsession.datecreated, SUM(introsession.quantity) as 'totalregistrant'
                            FROM introsession
                            LEFT JOIN center ON introsession.centerid = center.centerid
                            WHERE introsession.datecreated > '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
                            GROUP BY YEAR(introsession.datecreated), MONTH(introsession.datecreated)");
      $stmt->execute();
      $oneonone_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                            FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype != 2
                            GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated) ");
      $stmt->execute();
      $groupclass = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $stmt = $db->prepare("SELECT groupclassintrosession.datecreated, SUM(groupclassintrosession.quantity) as 'totalregistrant'
                            FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
                            GROUP BY YEAR(groupclassintrosession.datecreated), MONTH(groupclassintrosession.datecreated) ");
      $stmt->execute();
      $groupclass_f = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $months = array();
      for($n=0; $n<$monthsgap; $n++) {
        $months[] = array(
          "categories" => date("Y<\b\\r>M", strtotime($datefrom." +".$n." months")),
          "fulldate" => date("Y-m", strtotime($datefrom." +".$n." month")),
          "day" => date("d", strtotime($datefrom." +".$n." month"))
        );
      }

      echo json_encode(array(
        "oneonone" => $oneonone,
        "oneonone_f" => $oneonone_f,
        "groupclass" => $groupclass,
        "groupclass_f" => $groupclass_f,
        "months" => $months,
      ));

    } // } end of request isPost
  }

  public function oneononefranchiseAction() {
    $thisMonth = date('Y') ."-".date('m')."-01";
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT  introsession.datecreated,
                                    center.centertitle,
                                    center.centerstate,
                                    SUM(introsession.paid) as 'centerincome',
                                    SUM(introsession.quantity) as 'totalquantity',
                                    count(center.centerid) as 'noofregistrant' FROM introsession
                            LEFT JOIN center ON introsession.centerid = center.centerid
                            WHERE introsession.datecreated > '$thisMonth 00:00:00' AND center.centertype = 2
                            GROUP BY  YEAR(introsession.datecreated),
                                      MONTH(introsession.datecreated),
                                      DAY(introsession.datecreated),
                                      introsession.centerid
                            ");
      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $totalincome = 0;
        $totalregistrant = 0;
        $finalquantity = 0;
        foreach($sessions as $key => $value) {
            $totalincome = $totalincome + $value['centerincome'];
            $totalregistrant = $totalregistrant + $value['noofregistrant'];
            $finalquantity = $finalquantity + $value['totalquantity'];
        }

    $centerlist = Center::find(array("order"=>"centertitle"));
    $stateslist = States::find(array("order"=>"state"));

    echo json_encode(array(
      "thismonth" => $thisMonth,
      "sessions" => $sessions,
      "totalincome" => $totalincome,
      "totalregistrant" => $totalregistrant,
      "centers" => $centerlist->toArray(),
      "states" => $stateslist->toArray(),
      "finalquantity" => $finalquantity

    ));
  }

  public function filteroneononefranchiseAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $center = $request->getPost('center');
      $state = $request->getPost('state');

      $yearfrom = $request->getPost('fromyear');
      $yearto = $request->getPost('toyear');
      $monthfrom = $request->getPost('frommonth');
      $monthto = $request->getPost('tomonth');
      $datefrom = $yearfrom."-".$monthfrom."-01";
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;

      if($center == 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      } elseif ($center != 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerid = '$center' AND center.centertype = 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } elseif ($center == 12345 && $state != "ALL") {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centertype = 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } else {
        $stmt = $db->prepare("SELECT  introsession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(introsession.paid) as 'centerincome',
          SUM(introsession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM introsession
          LEFT JOIN center ON introsession.centerid = center.centerid
          WHERE introsession.datecreated >= '$datefrom 00:00:00' AND introsession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centerid = '$center' AND center.centertype = 2
          GROUP BY  YEAR(introsession.datecreated),
          MONTH(introsession.datecreated),
          DAY(introsession.datecreated),
          introsession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      }

      $totalincome = 0;
      $totalregistrant = 0;
      $finalquantity = 0;
      foreach($sessions as $key => $value) {
        $totalincome = $totalincome + $value['centerincome'];
        $totalregistrant = $totalregistrant + $value['noofregistrant'];
        $finalquantity = $finalquantity + $value['totalquantity'];
      }

      echo json_encode(array(
        "finalquantity" => $finalquantity,
        "dateto" => $dateto,
        "datefrom" => $datefrom,
        "sessions" => $sessions,
        "totalincome" => $totalincome,
        "totalregistrant" => $totalregistrant,
      ));
    }
  }

    public function allfranchiseAction() {
     $thisMonth = date('Y') ."-".date('m')."-01";
      $db = \Phalcon\DI::getDefault()->get('db');

      $stmt = $db->prepare("SELECT franchiseincome.*, center.centerid, center.centertitle, center.centerstate
                              FROM (
                                  SELECT  introsession.datecreated as datecreated,
                                          introsession.centerid as centerid,
                                          introsession.name as name,
                                          introsession.paid as centerincome,
                                          introsession.payment,
                                          'intro' as status
                                  FROM introsession
                                    UNION
                                  SELECT  groupclassintrosession.datecreated as datecreated,
                                          groupclassintrosession.centerid as centerid,
                                          groupclassintrosession.name as name,
                                          groupclassintrosession.paid as centerincome,
                                          groupclassintrosession.payment,
                                          'group' as status
                                  FROM groupclassintrosession
                                  ) AS franchiseincome
                                  LEFT JOIN center ON franchiseincome.centerid = center.centerid
                                  WHERE franchiseincome.datecreated > '$thisMonth 00:00:00' AND center.centertype = 2
                                  ORDER BY franchiseincome.datecreated
                          ");
      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      // var_dump($sessions);

        $totalincome = 0;
        $totalregistrant = 0;
        $finalquantity = 0;
        foreach($sessions as $key => $value) {
            $totalincome = $totalincome + $value['centerincome'];
            $totalregistrant = $totalregistrant + $value['noofregistrant'];
            $finalquantity = $finalquantity + $value['totalquantity'];
        }

    $centerlist = Center::find(array("order"=>"centertitle"));
    $stateslist = States::find(array("order"=>"state"));

    echo json_encode(array(
      "totalincome" => $totalincome,
      "thismonth" => $thisMonth,
      "sessions" => $sessions,
      "totalregistrant" => $totalregistrant,
      "centers" => $centerlist->toArray(),
      "states" => $stateslist->toArray(),
      "finalquantity" => $finalquantity
    ));
  }

  public function filterallfranchiseaction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $center = $request->getPost('center');
      $state = $request->getPost('state');

      $yearfrom = $request->getPost('fromyear');
      $yearto = $request->getPost('toyear');
      $monthfrom = $request->getPost('frommonth');
      $monthto = $request->getPost('tomonth');
      $datefrom = $yearfrom."-".$monthfrom."-01";
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;

      if($center == 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT franchiseincome.*, center.centerid, center.centertitle, center.centerstate
                              FROM (
                                  SELECT  introsession.datecreated as datecreated,
                                          introsession.centerid as centerid,
                                          introsession.name as name,
                                          introsession.paid as centerincome,
                                          introsession.payment,
                                          'intro' as status
                                  FROM introsession
                                    UNION
                                  SELECT  groupclassintrosession.datecreated as datecreated,
                                          groupclassintrosession.centerid as centerid,
                                          groupclassintrosession.name as name,
                                          groupclassintrosession.paid as centerincome,
                                          groupclassintrosession.payment,
                                          'group' as status
                                  FROM groupclassintrosession

                                  ) AS franchiseincome
                                  LEFT JOIN center ON franchiseincome.centerid = center.centerid
                                  WHERE franchiseincome.datecreated > '$datefrom 00:00:00' AND franchiseincome.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
                                  ORDER BY franchiseincome.datecreated

                          ");
      } elseif ($center != 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT franchiseincome.*, center.centerid, center.centertitle, center.centerstate
                              FROM (
                                  SELECT  introsession.datecreated as datecreated,
                                          introsession.centerid as centerid,
                                          introsession.name as name,
                                          introsession.paid as centerincome,
                                          introsession.payment,
                                          'intro' as status
                                  FROM introsession
                                    UNION
                                  SELECT  groupclassintrosession.datecreated as datecreated,
                                          groupclassintrosession.centerid as centerid,
                                          groupclassintrosession.name as name,
                                          groupclassintrosession.paid as centerincome,
                                          groupclassintrosession.payment,
                                          'group' as status
                                  FROM groupclassintrosession
                                  ) AS franchiseincome
                                  LEFT JOIN center ON franchiseincome.centerid = center.centerid
                                  WHERE franchiseincome.datecreated > '$datefrom 00:00:00' AND franchiseincome.datecreated <= '$dateto 23:59:59' AND center.centerid = '$center' AND center.centertype = 2
                                  ORDER BY franchiseincome.datecreated
                          ");
      } elseif ($center == 12345 && $state != "ALL") {
        $stmt = $db->prepare("SELECT franchiseincome.*, center.centerid, center.centertitle, center.centerstate
                              FROM (
                                  SELECT  introsession.datecreated as datecreated,
                                          introsession.centerid as centerid,
                                          introsession.name as name,
                                          introsession.paid as centerincome,
                                          introsession.payment,
                                          'intro' as status
                                  FROM introsession
                                    UNION
                                  SELECT  groupclassintrosession.datecreated as datecreated,
                                          groupclassintrosession.centerid as centerid,
                                          groupclassintrosession.name as name,
                                          groupclassintrosession.paid as centerincome,
                                          groupclassintrosession.payment,
                                          'group' as status
                                  FROM groupclassintrosession
                                  ) AS franchiseincome
                                  LEFT JOIN center ON franchiseincome.centerid = center.centerid
                                  WHERE franchiseincome.datecreated > '$datefrom 00:00:00' AND franchiseincome.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centertype = 2
                                  ORDER BY franchiseincome.datecreated
                          ");
      } else {
        $stmt = $db->prepare("SELECT franchiseincome.*, center.centerid, center.centertitle, center.centerstate
                              FROM (
                                  SELECT  introsession.datecreated as datecreated,
                                          introsession.centerid as centerid,
                                          introsession.name as name,
                                          introsession.paid as centerincome,
                                          introsession.payment,
                                          'intro' as status
                                  FROM introsession
                                    UNION
                                  SELECT  groupclassintrosession.datecreated as datecreated,
                                          groupclassintrosession.centerid as centerid,
                                          groupclassintrosession.name as name,
                                          groupclassintrosession.paid as centerincome,
                                          groupclassintrosession.payment,
                                          'group' as status
                                  FROM groupclassintrosession
                                  ) AS franchiseincome
                                  LEFT JOIN center ON franchiseincome.centerid = center.centerid
                                  WHERE franchiseincome.datecreated > '$datefrom 00:00:00' AND franchiseincome.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centerid = '$center' AND center.centertype = 2
                                  ORDER BY franchiseincome.datecreated

                          ");
      }

      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      $totalincome = 0;
      $totalregistrant = 0;
      $finalquantity = 0;
      foreach($sessions as $key => $value) {
        $totalincome = $totalincome + $value['centerincome'];
        $totalregistrant = $totalregistrant + $value['noofregistrant'];
        $finalquantity = $finalquantity + $value['totalquantity'];
      }
      echo json_encode(array(
        "finalquantity" => $finalquantity,
        "dateto" => $dateto,
        "datefrom" => $datefrom,
        "sessions" => $sessions,
        "totalincome" => $totalincome,
        "totalregistrant" => $totalregistrant,
      ));
    }
  }

  public function groupfranchiseAction() {
    $thisMonth = date('Y') ."-".date('m')."-01";
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
                                    center.centertitle,
                                    center.centerstate,
                                    SUM(groupclassintrosession.paid) as 'centerincome',
                                    SUM(groupclassintrosession.quantity) as 'totalquantity',
                                    count(groupclassintrosession.centerid) as 'noofregistrant' FROM groupclassintrosession
                            LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
                            WHERE groupclassintrosession.datecreated > '$thisMonth 00:00:00' AND center.centertype = 2
                            GROUP BY  YEAR(groupclassintrosession.datecreated),
                                      MONTH(groupclassintrosession.datecreated),
                                      DAY(groupclassintrosession.datecreated),
                                      groupclassintrosession.centerid
                            ");
      $stmt->execute();
      $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $totalincome = 0;
        $totalregistrant = 0;
        $finalquantity = 0;
        foreach($sessions as $key => $value) {
            $totalincome = $totalincome + $value['centerincome'];
            $totalregistrant = $totalregistrant + $value['noofregistrant'];
            $finalquantity = $finalquantity + $value['totalquantity'];
        }

    $centerlist = Center::find(array("order"=>"centertitle"));
    $stateslist = States::find(array("order"=>"state"));

    echo json_encode(array(
      "thismonth" => $thisMonth,
      "sessions" => $sessions,
      "totalincome" => $totalincome,
      "totalregistrant" => $totalregistrant,
      "centers" => $centerlist->toArray(),
      "states" => $stateslist->toArray(),
      "finalquantity" => $finalquantity
    ));
  }

  public function filtergroupfranchiseAction() {
    $db = \Phalcon\DI::getDefault()->get('db');
    $request = new \Phalcon\Http\Request();
    if($request->isPost()) {
      $center = $request->getPost('center');
      $state = $request->getPost('state');

      $yearfrom = $request->getPost('fromyear');
      $yearto = $request->getPost('toyear');
      $monthfrom = $request->getPost('frommonth');
      $monthto = $request->getPost('tomonth');
      $datefrom = $yearfrom."-".$monthfrom."-01";
      if($monthto=='01' || $monthto=='03' || $monthto=='05' || $monthto=='07' || $monthto=='08' || $monthto=='10' || $monthto=='12') {
            $dayto = 31;
        } else if ($monthto=='02') {
            if($yearto % 4 === 0) { $dayto = 29; }
            else { $dayto = 28; }
        } else {
            $dayto = 30;
        }
      $dateto = $yearto."-".$monthto."-".$dayto;

      if($center == 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centertype = 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);

      } elseif ($center != 12345 && $state == "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerid = '$center' AND center.centertype = 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } elseif ($center == 12345 && $state != "ALL") {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centertype = 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      } else {
        $stmt = $db->prepare("SELECT  groupclassintrosession.datecreated,
          center.centertitle,
          center.centerstate,
          SUM(groupclassintrosession.paid) as 'centerincome',
          SUM(groupclassintrosession.quantity) as 'totalquantity',
          count(center.centerid) as 'noofregistrant' FROM groupclassintrosession
          LEFT JOIN center ON groupclassintrosession.centerid = center.centerid
          WHERE groupclassintrosession.datecreated >= '$datefrom 00:00:00' AND groupclassintrosession.datecreated <= '$dateto 23:59:59' AND center.centerstate = '$state' AND center.centerid = '$center' AND center.centertype = 2
          GROUP BY  YEAR(groupclassintrosession.datecreated),
          MONTH(groupclassintrosession.datecreated),
          DAY(groupclassintrosession.datecreated),
          groupclassintrosession.centerid
          ");
        $stmt->execute();
        $sessions = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      }

      $totalincome = 0;
      $totalregistrant = 0;
      $finalquantity = 0;
      foreach($sessions as $key => $value) {
        $totalincome = $totalincome + $value['centerincome'];
        $totalregistrant = $totalregistrant + $value['noofregistrant'];
        $finalquantity = $finalquantity + $value['totalquantity'];
      }

      echo json_encode(array(
        "dateto" => $dateto,
        "datefrom" => $datefrom,
        "sessions" => $sessions,
        "totalincome" => $totalincome,
        "totalregistrant" => $totalregistrant,
        "finalquantity" => $finalquantity
      ));
    }
  }
}
