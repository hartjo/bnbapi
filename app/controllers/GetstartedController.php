<?php

namespace Controllers;

use \Models\News as News;
use \Models\Notification as Notification;
use \Models\Centercalendar as Centercalendar;
use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplace as Beneplace;
use \Models\Centeremail as Centeremail;
use \Models\Introsession as Introsession;
use \Models\Centernews as Centernews;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Centerpricingregclass as Centerpricingregclass;
use \Models\Centerpricingsessionfee as Centerpricingsessionfee;
use \Models\Centerhours as Centerhours;
use \Models\Centersociallinks as Centersociallinks;
use \Models\Center as Center;
use \Models\Centerimages as Centerimages;
use \Models\Centeroffer as Centeroffer;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Centersession1 as Centersession1;
use \Models\Centersession2 as Centersession2;
use \Models\Centersession3 as Centersession3;
use \Models\Centersession4 as Centersession4;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class GetstartedController extends \Phalcon\Mvc\Controller {

    public function searchlocationAction(){
        // $request = new \Phalcon\Http\Request();
        
        // if($request->isPost()){
        //     $state = $request->getPost('state');
        //     $city = $request->getPost('city');
        //     $zip = $request->getPost('zip');

        //     $db = \Phalcon\DI::getDefault()->get('db');
        //     $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerstate Like '%".$state."%' AND center.centercity Like '%".$city."%' AND center.centerzip Like '%".$zip."%'");

        //     $stmt->execute();
        //     $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        // }
        // echo json_encode($searchresult);
        $request = new \Phalcon\Http\Request();
        $cb = new CB;
        
        if($request->isPost()){


            $state = $request->getPost('state');
            $city = $request->getPost('city');
            $zip = $request->getPost('zip');

            if($zip != "" && $city == "" && $state == "")
            {
                $zipfrom = Cities_extended::findFirst('zip = "'.$zip.'"');
                if($zipfrom){
                   $searchresult = array();
                   $db = \Phalcon\DI::getDefault()->get('db');
                   $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid");

                   $stmt->execute();
                   $center = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                    $getdis = array();
                    $data = array();
                   foreach ($center as $key) {
                      if( $cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles')<= 50){
                        $getdis[] = $key['centerid'];
                      }
                   }

                   $fsearchresult = array();
                   foreach ($getdis as $centerid) {
                      
                        $db = \Phalcon\DI::getDefault()->get('db');
                        $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."'");

                        $stmt->execute();
                        $ssearchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                        foreach ($ssearchresult as $key) {
                      
                       
                            $data[] = array(
                              "centerid" => $key['centerid'],
                              "centertitle" =>  $key['centertitle'],
                              "centerslugs" =>  $key['centerslugs'],
                              "centerdesc" => $key['centerdesc'],
                              "openingdate" => $key['openingdate'],
                              "centerregion" =>$key['centerregion'],
                              "centerdistrict" => $key['centerdistrict'],
                              "centeraddress" => $key['centeraddress'],
                              "centerstate" => $key['centerstate'],
                              "centercity" => $key['centercity'],
                              "centerzip" => $key['centerzip'],
                              "metatitle" => $key['metatitle'],
                              "metadesc" => $key['metadesc'],
                              "centermanager" => $key['centermanager'],
                              "centertype" => $key['centertype'],
                              "status" => $key['status'],
                              "centertelnumber" => $key['centertelnumber'],
                              "authorizeid" => $key['authorizeid'],
                              "authorizekey" => $key['authorizekey'],
                              "paypalid" => $key['paypalid'],
                              "origsessionprice" => $key['origsessionprice'],
                              "dissessionprice" => $key['dissessionprice'],
                              "origgroupprice" => $key['origgroupprice'],
                              "disgroupprice" => $key['disgroupprice'],
                              "orig1monthprice" => $key['orig1monthprice'],
                              "dis1monthprice" => $key['dis1monthprice'],
                              "orig3monthprice" => $key['orig3monthprice'],
                              "dis3monthprice" => $key['dis3monthprice'],
                              "datecreated" => $key['datecreated'],
                              "dateedited" => $key['dateedited'],
                              "locationid" => $key['locationid'],
                              "lat" =>$key['lat'],
                              "lon" => $key['lon'],
                              "phonenumber" => $key['phonenumber'],
                              "email" => $key['email'],
                              "fdistance" => intval($cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles'))
                            );
                         }
                    }

                   $searchresult =  $data;
                }
            }
            else{


            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerstate Like '%".$state."%' AND center.centercity Like '%".$city."%' AND center.centerzip Like '%".$zip."%'");

            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            }
        }
        echo json_encode($searchresult);
    }

    public function paypalreturnAction(){
                // date_default_timezone_set('Asia/Taipei');
        
                // STEP 1: read POST data

                // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
                // Instead, read raw POST data from the input stream. 
                $raw_post_data = file_get_contents('php://input');
                $raw_post_array = explode('&', $raw_post_data);
                $myPost = array();
                foreach ($raw_post_array as $keyval) {
                  $keyval = explode ('=', $keyval);
                  if (count($keyval) == 2)
                     $myPost[$keyval[0]] = urldecode($keyval[1]);
                }
                // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
                $req = 'cmd=_notify-validate';
                if(function_exists('get_magic_quotes_gpc')) {
                   $get_magic_quotes_exists = true;
                } 
                foreach ($myPost as $key => $value) {        
                   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
                        $value = urlencode(stripslashes($value)); 
                   } else {
                        $value = urlencode($value);
                   }
                   $req .= "&$key=$value";
                }

                 
                // STEP 2: POST IPN data back to PayPal to validate

                $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
                curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

                // In wamp-like environments that do not come bundled with root authority certificates,
                // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set 
                // the directory path of the certificate as shown below:
                // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
                if( !($res = curl_exec($ch)) ) {
                    // error_log("Got " . curl_error($ch) . " when processing IPN data");
                    curl_close($ch);
                    exit;
                }
                curl_close($ch);
                 

                // STEP 3: Inspect IPN validation result and act accordingly

                if (strcmp ($res, "VERIFIED") == 0) {
                    // The IPN is verified, process it:
                    // check whether the payment_status is Completed
                    // check that txn_id has not been previously processed
                    // check that receiver_email is your Primary PayPal email
                    // check that payment_amount/payment_currency are correct
                    // process the notification
                    $guid = new \Utilities\Guid\Guid();
                    $sessionid = $guid->GUID();
                    $notificationid = $guid->GUID();
                                        
                    $data = explode('|', $_POST['custom']);
                    $name = $data[0];
                    $email = $data[1];
                    $centerid = $data[2];
                    $phone = $data[3];
                    $hour = $data[4];
                    $day = $data[5];
                    $comment = $data[6];
                    $qty = $data[7];
                    $device = $data[8];
                    $deviceos = $data[9];
                    $devicebrowser = $data[10];
                    $sessionclass = $data[12];
                    $sessiontype = $data[13];
                    $moduletype = $data[14];
                    // assign posted variables to local variables
                    $item_name = $_POST['item_name'];
                    $item_number = $_POST['item_number'];
                    $payment_status = $_POST['payment_status'];
                    $payment_amount = $_POST['mc_gross'];
                    $payment_currency = $_POST['mc_currency'];
                    $txn_id = $_POST['txn_id'];
                    $receiver_email = $_POST['receiver_email'];
                    $payer_email = $_POST['payer_email'];


                    $dates = explode("/", $day);
                    $d = $dates[2].'-'.$dates[0].'-'.$dates[1];

                    $db = \Phalcon\DI::getDefault()->get('db');
                    $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$centerid."' ");

                    $stmt->execute();
                    $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);

                    if($moduletype == 'starter'){
                            if($sessiontype == 0){
                                $session = new Introsession();
                                $session->assign(array(
                                    'sessionid' => $sessionid,
                                    'centerid' => $centerid,
                                    'confirmationnumber' => $txn_id,
                                    'name' => $name,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'day' => $d,
                                    'hour' => $hour,
                                    'comment' => $comment,
                                    'quantity' => $qty,
                                    'device' => $device,
                                    'deviceos' => $deviceos,
                                    'devicebrowser' => $devicebrowser,
                                    'datecreated' => date('Y-m-d H:i:s'),
                                    'centersessionstatus' => 0,
                                    'regionsessionstatus' => 0,
                                    'adminsessionstatus' => 0,
                                    'payment' => 'Paypal',
                                    'paid' => $payment_amount


                                    ));
                                if (!$session->save()) {
                                    $errors = array();
                                    foreach ($session->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    echo json_encode(array('error' => $errors));
                                } 
                                else {


                                    $notification = new Notification();
                                    $notification->assign(array(
                                    'notificationid' => $notificationid,
                                    'centerid' => $centerid,
                                    'itemid' => $sessionid,
                                    'name' => $name,
                                    'datecreated' => date('Y-m-d H:i:s'),
                                    'adminstatus' => 0,
                                    'regionstatus' => 0,
                                    'centerstatus' => 0,
                                    'type' => 'introsession'
                                    ));
                                    if ($notification->save()) {

                                    } 



                                    $data['msg'] = "Location successfully saved!";
                                    $data['type'] = "success";
                                    $dc = new CB();
                                    $body = '<html>
                                            <head>
                                                <title></title>
                                                <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                            </head>
                                            <body style="font-family: "Droid Sans";
                                            background: #fff;">
                                            <style>
                                                body {
                                                    font-family: "Droid Sans"  !important;
                                                    background: #fff  !important;
                                                }

                                            </style>
                                            <div style="margin: auto !important;
                                            width: 980px !important;">
                                            <div >
                                                <div >
                                                    <div >
                                                        <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                            <div style="width: 100% !important;height: 60px;">
                                                                <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                                <p style="color:red;font-size:23px;">Your Appointment</p>
                                                                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $name .'</span></p>
                                                                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($d)).','.$hour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                                <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                    <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                    <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                                </p>
                                                                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                                                                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$payment_amount.' Paypal</span></p>
                                                                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </body>
                                        </html>';

                                    $send = $dc->sendMail($email,'BodynBrain : Appointment for '.$searchresult['centertitle'].' Center',$body);
                                    $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $name .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($day)).','.$hour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$payment_amount.')
                                                <br>
                                                7. Application Code: '.$txn_id.'
                                                <br>
                                                8.Type: '.$sessionclass.'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);

                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }

                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }

                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '(1-on-1 Intro Appt.:Paid($'.$payment_amount.'))/Name:'.$name.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$hour.'. Confirm it. Check email '. $comment;


                                    $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );

                                    
                                
                                }
                            }
                            else{

                                $session = new groupclassintrosession();
                                $session->assign(array(
                                    'sessionid' => $sessionid,
                                    'centerid' => $centerid,
                                    'confirmationnumber' => $txn_id,
                                    'name' => $name,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'day' => $d,
                                    'hour' => $hour,
                                    'comment' => $comment,
                                    'quantity' => $qty,
                                    'device' => $device,
                                    'deviceos' => $deviceos,
                                    'devicebrowser' => $devicebrowser,
                                    'datecreated' => date('Y-m-d H:i:s'),
                                    'centersessionstatus' => 0,
                                    'regionsessionstatus' => 0,
                                    'adminsessionstatus' => 0,
                                    'payment' => 'Paypal',
                                    'paid' => $payment_amount

                                    ));
                                if (!$session->save()) {
                                    $errors = array();
                                    foreach ($session->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    echo json_encode(array('error' => $errors));
                                } 
                                else {

                                     $notification = new Notification();
                                     $notification->assign(array(
                                        'notificationid' => $notificationid,
                                        'centerid' => $centerid,
                                        'itemid' => $sessionid,
                                        'name' => $name,
                                        'datecreated' => date('Y-m-d H:i:s'),
                                        'adminstatus' => 0,
                                        'regionstatus' => 0,
                                        'centerstatus' => 0,
                                        'type' => 'groupsession'
                                        ));
                                     if ($notification->save()) {

                                     } 






                                    $data['msg'] = "Location successfully saved!";
                                    $data['type'] = "success";
                                    $dc = new CB();
                                   $body = '<html>
                                            <head>
                                                <title></title>
                                                <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                            </head>
                                            <body style="font-family: "Droid Sans";
                                            background: #fff;">
                                            <style>
                                                body {
                                                    font-family: "Droid Sans"  !important;
                                                    background: #fff  !important;
                                                }

                                            </style>
                                            <div style="margin: auto !important;
                                            width: 980px !important;">
                                            <div >
                                                <div >
                                                    <div >
                                                        <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                            <div style="width: 100% !important;height: 60px;">
                                                                <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                                <p style="color:red;font-size:23px;">Your Appointment</p>
                                                                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $name .'</span></p>
                                                                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($d)).','.$hour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                                <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                    <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                    <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                                </p>
                                                                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                                                                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$payment_amount.' Paypal</span></p>
                                                                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </body>
                                        </html>';

                                    $send = $dc->sendMail($email,'BodynBrain : Appointment for '.$searchresult['centertitle'].' Center',$body);

                                    $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $name .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($day)).','.$hour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$payment_amount.')
                                                <br>
                                                7. Application Code: '.$txn_id.'
                                                <br>
                                                8.Type: '.$sessionclass.'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);


                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }


                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }


                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '(1-on-1 Intro + 1 Group:Paid($'.$payment_amount.'))/Name:'.$name.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$hour.'. Confirm it. Check email '. $comment;


                                     $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );

                                
                                }

                            }

                    } 
                    else if($moduletype == 'beneplace'){

                            $beneplace = new Beneplace();
                                $beneplace->assign(array(
                                    'sessionid' => $sessionid,
                                    'centerid' => $centerid,
                                    'confirmationnumber' => $txn_id,
                                    'name' => $name,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'day' => $d,
                                    'hour' => $hour,
                                    'comment' => $comment,
                                    'program' => $sessionclass,
                                    'quantity' => $qty,
                                    'device' => $device,
                                    'deviceos' => $deviceos,
                                    'devicebrowser' => $devicebrowser,
                                    'datecreated' => date('Y-m-d H:i:s'),
                                    'centersessionstatus' => 0,
                                    'regionsessionstatus' => 0,
                                    'adminsessionstatus' => 0,
                                    'payment' => 'Paypal',
                                    'paid' => $payment_amount

                                    ));
                                if (!$beneplace->save()) {
                                    $errors = array();
                                    foreach ($beneplace->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    echo json_encode(array('error' => $errors));
                                } 
                                else {


                                    $notification = new Notification();
                                    $notification->assign(array(
                                        'notificationid' => $notificationid,
                                        'centerid' => $centerid,
                                        'itemid' => $sessionid,
                                        'name' => $name,
                                        'datecreated' => date('Y-m-d H:i:s'),
                                        'adminstatus' => 0,
                                        'regionstatus' => 0,
                                        'centerstatus' => 0,
                                        'type' => 'beneplace'
                                        ));
                                    if ($notification->save()) {

                                    } 









                                    $data['msg'] = "Location successfully saved!";
                                    $data['type'] = "success";
                                    $dc = new CB();
                                    $body = '<html>
                                            <head>
                                                <title></title>
                                                <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                            </head>
                                            <body style="font-family: "Droid Sans";
                                            background: #fff;">
                                            <style>
                                                body {
                                                    font-family: "Droid Sans"  !important;
                                                    background: #fff  !important;
                                                }

                                            </style>
                                            <div style="margin: auto !important;
                                            width: 980px !important;">
                                            <div >
                                                <div >
                                                    <div >
                                                        <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                            <div style="width: 100% !important;height: 60px;">
                                                                <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                                <p style="color:red;font-size:23px;">Your Appointment</p>
                                                                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $name .'</span></p>
                                                                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($d)).','.$hour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                                <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                    <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                    <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                                </p>
                                                                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                                                                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$payment_amount.' Paypal</span></p>
                                                                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </body>
                                        </html>';

                                    $send = $dc->sendMail($email,'BodynBrain : Appointment for '.$searchresult['centertitle'].' Center',$body);

                                    $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $name .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($day)).','.$hour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$payment_amount.')
                                                <br>
                                                7. Application Code: '.$txn_id.'
                                                <br>
                                                8.Type: '.$sessionclass.'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);


                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }


                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }

                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '('.$sessionclass.' Paid($'.$payment_amount.'))/Name:'.$name.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$hour.'. Confirm it. Check email '. $comment;

                                    $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );





                                    
                                
                                }


                    } //end of moduletype

                    // IPN message values depend upon the type of notification sent.
                    // To loop through the &_POST array and print the NV pairs to the screen:
                    foreach($_POST as $key => $value) {
                      echo $key." = ". $value."<br>";
                    }
                } else if (strcmp ($res, "INVALID") == 0) {
                    // IPN invalid, log for manual investigation
                    echo "The response from IPN was: <b>" .$res ."</b>";
                }
    }

    public function introsessionlistAction($num, $page, $keyword,$centerid){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM introsession LEFT JOIN center ON introsession.centerid = center.centerid WHERE introsession.centerid = '" .$centerid. "' ORDER BY introsession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE introsession.centerid = '" .$centerid. "' ORDER BY introsession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid  WHERE introsession.centerid = '" .$centerid. "' and introsession.name LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.quantity LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.payment LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT introsession.centerid FROM   introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE introsession.centerid = '" .$centerid. "' and introsession.name LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.quantity LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.payment LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalitem = count($searchresult1);
            
        }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }


    public function authorizeAction(){
        // date_default_timezone_set('Asia/Taipei');
        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){

            $cardnumber = $request->getPost('cardnumber');
            $cardtype = $request->getPost('cardtype');
            $expiyear = $request->getPost('expiyear');
            $expimonth = $request->getPost('expimonth');
            $firstname = $request->getPost('firstname');
            $lastname = $request->getPost('lastname');
            $itemprice = $request->getPost('itemprice');
            $fullname = $request->getPost('fullname');
            $phone = $request->getPost('phone');
            $email = $request->getPost('email');
            $qty = $request->getPost('qty');
            $classday = $request->getPost('classday');
            $classhour = $request->getPost('classhour');
            $comment = $request->getPost('comment');
            $device = $request->getPost('device');
            $deviceos = $request->getPost('deviceos');
            $devicebrowser = $request->getPost('devicebrowser');
            $centerid = $request->getPost('centerid');
            $billfullname = $request->getPost('billfullname');
            $billstate = $request->getPost('billstate');
            $billzip = $request->getPost('billzip');
            $billcity = $request->getPost('billcity');
            $billadd1 = $request->getPost('billadd1');
            $billadd2 = $request->getPost('billadd2');
            $billphone = $request->getPost('billphone');
            $itemprice = $request->getPost('itemprice');
            $totalprice = $request->getPost('totalprice');
            $sessionclass = $request->getPost('sessionclass');
            $sessiontype = $request->getPost('sessiontype');
            $authorizeid = $request->getPost('authorizeid');
            $authorizekey = $request->getPost('authorizekey');
            $moduletype = $request->getPost('moduletype');

            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$centerid."' ");

            $stmt->execute();
            $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);

            $stmt1 = $db->prepare("SELECT * FROM center LEFT JOIN centeremail ON center.centerid = centeremail.centerid  WHERE center.centerid = '$centerid' ");
            $stmt1->execute();
            $thisCenter = $stmt1->fetch(\PDO::FETCH_ASSOC);
            $centeremail = $thisCenter['email'];
            


            $post_url = "https://secure.authorize.net/gateway/transact.dll";

            $post_values = array(
                
                "x_login"           => $authorizeid,
                "x_tran_key"        => $authorizekey,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",

                "x_type"            => "AUTH_CAPTURE",
                "x_method"          => "CC",
                "x_card_num"        => $cardnumber,
                "x_exp_date"        => $expimonth.$expiyear,

                "x_amount"          => $totalprice,
                "x_description"     => $sessionclass,

                "x_first_name"      => $firstname,
                "x_last_name"       => $lastname,
                "x_address"         => $billadd1,
                "x_state"           => $billstate,
                "x_zip"             => $billzip
                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            $post_string = "";
            foreach( $post_values as $key => $value )
                { $post_string .= "$key=" . urlencode( $value ) . "&"; }
            $post_string = rtrim( $post_string, "& " );

            $request = curl_init($post_url); // initiate curl object
                curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
                curl_setopt($request, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
                curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
                $post_response = curl_exec($request); // execute curl post and store results in $post_response
                // additional options may be required depending upon your server configuration
                // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close ($request); // close curl object

            $dates = explode("/", $classday);
            $d = $dates[2].'-'.$dates[0].'-'.$dates[1];

            $response_array = explode($post_values["x_delim_char"],$post_response);

            $successpresponse = $response_array[0];

            $guid = new \Utilities\Guid\Guid();
            $sessionid = $guid->GUID();
            $notificationid = $guid->GUID();

            if($successpresponse=='1'){
                if($moduletype == 'starter'){
                     if($sessiontype == 0){
                                    $session = new Introsession();
                                    $session->assign(array(
                                        'sessionid' => $sessionid,
                                        'centerid' => $centerid,
                                        'confirmationnumber' => $response_array[6],
                                        'name' => $fullname,
                                        'email' => $email,
                                        'phone' => $phone,
                                        'day' => $d,
                                        'hour' => $classhour,
                                        'comment' => $comment,
                                        'device' => $device,
                                        'deviceos' => $deviceos,
                                        'devicebrowser' => $devicebrowser,
                                        'quantity' => $qty,
                                        'datecreated' => date('Y-m-d H:i:s'),
                                        'centersessionstatus' => 0,
                                        'regionsessionstatus' => 0,
                                        'adminsessionstatus' => 0,
                                        'payment' => $response_array[51],
                                        'paid' => $totalprice

                                    ));

                                    if (!$session->save()) {
                                        $errors = array();
                                        foreach ($session->getMessages() as $message) {
                                            $errors[] = $message->getMessage();
                                        }
                                        echo json_encode(array('error' => $errors));
                                    } 

                                    else {

                                        $notification = new Notification();
                                        $notification->assign(array(
                                            'notificationid' => $notificationid,
                                            'centerid' => $centerid,
                                            'itemid' => $sessionid,
                                            'name' => $fullname,
                                            'datecreated' => date('Y-m-d H:i:s'),
                                            'adminstatus' => 0,
                                            'regionstatus' => 0,
                                            'centerstatus' => 0,
                                            'type' => 'introsession'
                                            ));
                                        if ($notification->save()) {

                                        } 

                                        $data['msg'] = "Location successfully saved!";
                                        $data['type'] = "success";

                                        $dc = new CB();
                        $useremail = '<html>
                                    <head>
                                    <title></title>
                                    <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                    </head>
                                    <body style="font-family: "Droid Sans";background: #fff;">
                                    <style>
                                        body {
                                            font-family: "Droid Sans"  !important;
                                            background: #fff  !important;
                                        }

                                    </style>
                                    <div style="margin: auto !important;width: 980px !important;">
                                        <div >
                                            <div >
                                                <div >
                                                    <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                        <div style="width: 100% !important;height: 60px;">
                                                            <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                            <p style="color:red;font-size:23px;">Your Appointment</p>
                                                            <p style="color: #999 !important">Name: <span  style="color: #000;">'. $fullname .'</span></p>
                                                            <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($classday)).','.$classhour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                            <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                            </p>
                                                            <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$response_array[6].'</span></p>
                                                            <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$totalprice.' '.$response_array[51].'</span></p>
                                                            <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </body>
                                    </html>';

                        $senduseremail = $dc->sendMail($email,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$useremail);


                        $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $fullname .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($classday)).','.$classhour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$response_array[9].')
                                                <br>
                                                7. Application Code: '.$response_array[6].'
                                                <br>
                                                8.Type: '.$response_array[8].'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);


                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }


                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }

                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '(1-on-1 Intro Appt.:Paid($'.$totalprice.'))/Name:'.$fullname.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$classhour.'. Confirm it. Check email '. $comment;


                                     $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );



                           
                                    }
                            }
                            else {
                                   $session = new groupclassintrosession();
                                   $session->assign(array(
                                    'sessionid' => $sessionid,
                                    'centerid' => $centerid,
                                    'confirmationnumber' => $response_array[6],
                                    'name' => $fullname,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'day' => $d,
                                    'hour' => $classhour,
                                    'comment' => $comment,
                                    'device' => $device,
                                    'deviceos' => $deviceos,
                                    'devicebrowser' => $devicebrowser,
                                    'quantity' => $qty,
                                    'datecreated' => date('Y-m-d H:i:s'),
                                    'centersessionstatus' => 0,
                                    'regionsessionstatus' => 0,
                                    'adminsessionstatus' => 0,
                                    'payment' => $response_array[51],
                                    'paid' => $totalprice

                                    ));
                                   if (!$session->save()) {
                                    $errors = array();
                                    foreach ($session->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    echo json_encode(array('error' => $errors, "response" => $response_array));
                                    } 
                                    else {


                                        $notification = new Notification();
                                        $notification->assign(array(
                                            'notificationid' => $notificationid,
                                            'centerid' => $centerid,
                                            'itemid' => $sessionid,
                                            'name' => $fullname,
                                            'datecreated' => date('Y-m-d H:i:s'),
                                            'adminstatus' => 0,
                                            'regionstatus' => 0,
                                            'centerstatus' => 0,
                                            'type' => 'groupsession'
                                            ));
                                        if ($notification->save()) {

                                        } 

                                        $data['msg'] = "Location successfully saved!";
                                        $data['type'] = "success";
                                        
                                        $dc = new CB();
                        $useremail = '<html>
                                    <head>
                                    <title></title>
                                    <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                    </head>
                                    <body style="font-family: "Droid Sans";background: #fff;">
                                    <style>
                                        body {
                                            font-family: "Droid Sans"  !important;
                                            background: #fff  !important;
                                        }

                                    </style>
                                    <div style="margin: auto !important;width: 980px !important;">
                                        <div >
                                            <div >
                                                <div >
                                                    <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                        <div style="width: 100% !important;height: 60px;">
                                                            <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                            <p style="color:red;font-size:23px;">Your Appointment</p>
                                                            <p style="color: #999 !important">Name: <span  style="color: #000;">'. $fullname .'</span></p>
                                                            <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($classday)).','.$classhour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                            <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                            </p>
                                                            <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$response_array[6].'</span></p>
                                                            <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$totalprice.' '.$response_array[51].'</span></p>
                                                            <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </body>
                                    </html>';

                        $senduseremail = $dc->sendMail($email,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$useremail);


                        $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $fullname .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($classday)).','.$classhour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$response_array[9].')
                                                <br>
                                                7. Application Code: '.$response_array[6].'
                                                <br>
                                                8.Type: '.$response_array[8].'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);


                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }

                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }

                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '(1-on-1 Intro + 1 Group:Paid($'.$totalprice.'))/Name:'.$fullname.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$classhour.'. Confirm it. Check email '. $comment;


                                   $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );

                                    }
                                }
                }
                else{
                    $session = new Beneplace();
                    $session->assign(array(
                        'sessionid' => $sessionid,
                        'centerid' => $centerid,
                        'confirmationnumber' => $response_array[6],
                        'name' => $fullname,
                        'email' => $email,
                        'phone' => $phone,
                        'day' => $d,
                        'hour' => $classhour,
                        'comment' => $comment,
                        'device' => $device,
                        'deviceos' => $deviceos,
                        'devicebrowser' => $devicebrowser,
                        'program' => $sessionclass,
                        'quantity' => $qty,
                        'datecreated' => date('Y-m-d H:i:s'),
                        'centersessionstatus' => 0,
                        'regionsessionstatus' => 0,
                        'adminsessionstatus' => 0,
                        'payment' => $response_array[51],
                        'paid' => $totalprice

                        ));
                    if (!$session->save()) {
                        $errors = array();
                        foreach ($session->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    } 

                    else {


                        $notification = new Notification();
                        $notification->assign(array(
                            'notificationid' => $notificationid,
                            'centerid' => $centerid,
                            'itemid' => $sessionid,
                            'name' => $fullname,
                            'datecreated' => date('Y-m-d H:i:s'),
                            'adminstatus' => 0,
                            'regionstatus' => 0,
                            'centerstatus' => 0,
                            'type' => 'beneplace'
                            ));
                        if ($notification->save()) {

                        } 











                        $data['msg'] = "Location successfully saved!";
                        $data['type'] = "success";
                        
                        $dc = new CB();
                        $useremail = '<html>
                                    <head>
                                    <title></title>
                                    <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                    </head>
                                    <body style="font-family: "Droid Sans";background: #fff;">
                                    <style>
                                        body {
                                            font-family: "Droid Sans"  !important;
                                            background: #fff  !important;
                                        }

                                    </style>
                                    <div style="margin: auto !important;width: 980px !important;">
                                        <div >
                                            <div >
                                                <div >
                                                    <div style="width: 800px;border: solid 1px #999 !important;margin:auto;padding:20px;" >
                                                        <div style="width: 100% !important;height: 60px;">
                                                            <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif" style="width: 240px !important;height: auto!important;float: right !important;"></div>
                                                            <p style="color:red;font-size:23px;">Your Appointment</p>
                                                            <p style="color: #999 !important">Name: <span  style="color: #000;">'. $fullname .'</span></p>
                                                            <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($classday)).','.$classhour.'</span><em>(Your appointment is subject to availability.)</em></p>
                                                            <p style="color: #999 !important">Where: <span class="bluetext">'.$searchresult['centertitle'].'</span><br>
                                                                <em>'.$searchresult['centeraddress'].', '.$searchresult['centerzip'].'</em><br>
                                                                <em>Phone: '.$searchresult['phonenumber'].'</em>
                                                            </p>
                                                            <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$response_array[6].'</span></p>
                                                            <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$totalprice.' '.$response_array[51].'</span></p>
                                                            <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$searchresult['lat'].', '.$searchresult['lon'].'&zoom=14&size=900x300&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$searchresult['lat'].', '.$searchresult['lon'].'" width="100%" height="auto">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </body>
                                    </html>';

                        $senduseremail = $dc->sendMail($email,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$useremail);


                        $centeremail = '<html>
                                        <head>
                                        <title></title>
                                        <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
                                        </head>
                                        <style>
                                            body {
                                                font-family: "Droid Sans"  !important;
                                                background: #fff  !important;
                                            }
                                        </style>

                                        <body>
                                            <div style="display:block; width:90%; margin:auto;">
                                                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                                                    <center>
                                                        <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
                                                    </center>
                                                </div>
                                                <br>
                                                <br>
                                                <b>BodynBrain.com</b><br>
                                                Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                                                <br>
                                                1. Name: '. $fullname .'
                                                <br>
                                                2. Phone: '.$phone.'
                                                <br>
                                                3. Email: '.$email.'
                                                <br>
                                                4. Appointment Date: '.date("D, F d Y", strtotime($classday)).','.$classhour.'
                                                <br>
                                                <p>5. Comment: '.$comment.'
                                                </p>
                                                <br>
                                                6. Payment: Paid ($'.$response_array[9].')
                                                <br>
                                                7. Application Code: '.$response_array[6].'
                                                <br>
                                                8.Type: '.$response_array[8].'
                                                <br>
                                                <br>
                                                Please contact this person to confirm the appointment as soon as possible.
                                                <br>
                                                Thank you
                                            </div>
                                        </body>
                                    </html>';

                                    // SENDING EMAIL FOR CENTER
                                    $sendcenteremail = $dc->sendMail($searchresult['email'],'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);


                                    $DI = \Phalcon\DI::getDefault();
                                    $app = $DI->get('application');
                                    foreach($app->config->email as $bnbemail) {
                                        $send = $dc->sendMail($bnbemail,'BodyandBrain :Appointment for '.$searchresult['centertitle'].' Center',$centeremail);
                                    }


                                    $emailcarrier = '';
                                    if($searchresult['carrier'] == '1'){
                                        $emailcarrier = '@vtext.com';
                                    }
                                    else if($searchresult['carrier'] == '2'){
                                        $emailcarrier = '@txt.att.net';
                                    }
                                    else if($searchresult['carrier'] == '3'){
                                        $emailcarrier = '@messaging.sprintpcs.com';
                                    }
                                    else if($searchresult['carrier'] == '4'){
                                        $emailcarrier = '@tmomail.net';
                                    }
                                    else if($searchresult['carrier'] == '5'){
                                        $emailcarrier = '@vmobl.com';
                                    }
                                    else if($searchresult['carrier'] == '6'){
                                        $emailcarrier = '@mymetropcs.com';
                                    }

                                    $emailnumber = $searchresult['phone1'].$searchresult['phone2'].$searchresult['phone3'].$emailcarrier;
                                    // $emailnumber = 'llarenasjanrainier@gmail.com';
                                    $textmsg = '('.$response_array[8].' Paid($'.$totalprice.'))/Name:'.$fullname.'/Phone:'.$phone.'/Date:'.$d.'/Hour:'.$classhour.'. Confirm it. Check email '. $comment;


                                   $sendsmsemail = $dc->sendSms($emailnumber,'BodyandBrain',$textmsg );




                    }


                    
                }


                echo json_encode($response_array);
            } //end of if successpresponse
            else {
                echo json_encode($response_array);
            }
        }
    }

    public function getcenterdetailsAction($centerid){
         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."' ");

         $stmt->execute();
         $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
         echo json_encode(array('data' => $searchresult));
    }

    public function loadsessionAction($sessionid,$userid){
        $usertype = Users:: findFirst('id="'.$userid.'"');
        $changestatus = Introsession::findFirst("sessionid = '".$sessionid."'");
        if($changestatus){
            if($usertype->task == 'Administrator'){
                $changestatus->adminsessionstatus = 1;
            }
            else if(($usertype->task == 'Center Manager')){
                $changestatus->centersessionstatus = 1;
            }
            
            if (!$changestatus->save()) {
                $errors = array();
                foreach ($changestatus->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "status successfully saved!";
                $data['type'] = "success";

            }


        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM introsession WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }


     public function groupclasssessionlistAction($num, $page, $keyword,$centerid){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid = center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' ORDER BY groupclassintrosession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' ORDER BY groupclassintrosession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } 
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid  WHERE groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.name LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.quantity LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM   groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.name LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.quantity LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function loadgroupsessionAction($sessionid,$userid){
        $usertype = Users:: findFirst('id="'.$userid.'"');
        $changestatus = Groupclassintrosession::findFirst("sessionid = '".$sessionid."'");
        if($changestatus){
            if($usertype->task == 'Administrator'){
                $changestatus->adminsessionstatus = 1;
            }
            else if(($usertype->task == 'Center Manager')){
                $changestatus->centersessionstatus = 1;
            }
            if (!$changestatus->save()) {
                $errors = array();
                foreach ($changestatus->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "status successfully saved!";
                $data['type'] = "success";

            }


        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM groupclassintrosession WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }

    public function loadcenterAction($centerid){
       $db = \Phalcon\DI::getDefault()->get('db');
       $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."'");

       $stmt->execute();
       $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);

       echo json_encode($searchresult);
    }


    public function allintrosessionlistAction($num, $page, $keyword){

 if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,introsession.datecreated as 'classdatecreated' FROM introsession LEFT JOIN center ON introsession.centerid = center.centerid ORDER BY introsession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid ORDER BY introsession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT *,introsession.datecreated as 'classdatecreated' FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid  WHERE  introsession.name LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.quantity LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsession.payment LIKE '%" . $keyword . "%' or  introsession.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE  introsession.name LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.quantity LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsession.payment LIKE '%" . $keyword . "%' or  introsession.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalitem = count($searchresult1);
            
        }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function allgroupclasssessionlistAction($num, $page, $keyword){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosession.datecreated as 'classdatecreated' FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid = center.centerid ORDER BY groupclassintrosession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid ORDER BY groupclassintrosession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } 
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosession.datecreated as 'classdatecreated' FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid  WHERE  groupclassintrosession.name LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.quantity LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM   groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE  groupclassintrosession.name LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.quantity LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }

      
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    
   
}