<?php

namespace Controllers;

use \Models\Center as Center;
use \Models\States as States;
use \Models\Testimonies as Testimonies;

class IndexController extends \Phalcon\Mvc\Controller {
	public function FEindexPropertiesAction() {

		$db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt1 = $db1->prepare("SELECT testimonies.*, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.status = 1 AND testimonies.spotlight = 1 LIMIT 10 ");
        $stmt1->execute();
        $spotlight_stories = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        foreach($spotlight_stories as $key => $value) {
        	 $spotlight_stories[$key]['testimonies.subject'] = mysqli_real_escape_string($spotlight_stories[$key]['testimonies.subject']);
        	 $spotlight_stories[$key]['testimonies.metadesc'] = mysqli_real_escape_string($spotlight_stories[$key]['testimonies.metadesc']);
        	 $spotlight_stories[$key]['testimonies.details'] = mysqli_real_escape_string($spotlight_stories[$key]['testimonies.details']);
        }

        $states = States::find(array("order" => "state ASC"));
		$data = array();
		$centerarray = array();
		foreach($states as $state) {
			
			$st_code = $state->state_code;
			$centers = Center::find("centerstate = '$st_code' AND status = 1 ORDER BY centertitle ASC");
			foreach($centers as $center) {
				$centerarray = array(
					'centertitle' =>$center->centertitle,
					'centerslugs' =>$center->centerslugs,
					'centertype' => $center->centertype,
					'centercity' => $center->centercity
					);

				$data[] = array(
	                'state' => $state->state,
	                'center' => $centerarray
                	);
		
			}
		}

        echo json_encode(array(
        	"desc" => $spotlight_stories[0]['metadesc'],
        	"spotlights" => $spotlight_stories,
        	"data" => $data
        ));
	}
}
