<?php
namespace Controllers;
use \Models\Visitor as Visitor;


class VisitController extends \Phalcon\Mvc\Controller
{


    public function visitAction($ip)
    {


        $savenewvisitor=0;
        $newvisit=0;
        $note="";
        $dbdate='';

        $tabledata=array(
            "0" =>"ipaddress",
            "1" =>"lastvisitdate"
            );
        $datetoday=date("Y/m/d");//Current Date


        function newvisitor($ipadd,$tabledata,$date){
            $visit = new Visitor();
            $visit->assign(array(
                ''.$tabledata[0].''  => $ipadd,
                ''.$tabledata[1].''  => $date
                ));

            if (!$visit->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }else {
                $data = array('success' => 'New Visitor has Been save');
            }
            return $data;
        }
        $getvisitor= Visitor::find();
        $countdata= count($getvisitor); 
        foreach ($getvisitor as $data) {
            if($ip==$data->$tabledata[0]){
                $savenewvisitor++;
                $dbdate=$data->$tabledata[1];
            } 
        }
        if ($savenewvisitor==0) {
            newvisitor($ip,$tabledata,$datetoday);
            $note="Save New Data";
        }else{
         $exp_date = $dbdate;
         $todays_date = date("Y-m-d");
         $today = strtotime($todays_date);
         $expiration_date = strtotime($exp_date);

         if ($expiration_date < $today) {
            newvisitor($ip,$tabledata,$datetoday);
            $note="Save New Data";
        }else{
            $note="Nothing to do";
        }

    }
    echo json_encode($note);
    
}
public function countAction(){
    $count=0;
    $visitors= Visitor::find();
    $count=count($visitors);
    echo json_encode($count);
}


public function diagramAction(){

    $cmonth = substr(date("m.d.y"),0,2) + 1; 
    $count=0;
    for ($x =1; $x <= 12; $x++){
        $visitors= Visitor::find();
        foreach($visitors as $visit){
            $dbmonth = date('m', strtotime($visit->lastvisitdate));
            if($cmonth==$dbmonth){
                $count++;
            }else{$count=0;}
        }
        $data[]=$count;
        if($cmonth>11){
            $cmonth=0;
        }
        $cmonth++;
    }
    echo json_encode($data);   
}   

public function sysperAction()
{

    $tabledata=array(
        "0" =>"memoryusage",
        "1" =>"cpuusage",
        "2" =>"date",
        "3" =>"time"
        );
    $timestamp  =  strftime("%Y-%m-%d %H:%M:%S %Y");
    $time =strftime("%H:%M:%S", strtotime($timestamp))."\n";
    $date =strftime("%Y-%m-%d", strtotime($timestamp))."\n";

    $system= System::find();

    foreach ($system as $data) {
        $data= array(
            'memory'    => round($data->$tabledata[0],2),
            'cpu'       => $data->$tabledata[1],

            );
        $sysdata[]=$data;
    }
    echo json_encode($sysdata);  
}
}

